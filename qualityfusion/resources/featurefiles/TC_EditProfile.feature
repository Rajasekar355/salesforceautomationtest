@UCXN-14069
Feature: Feedback Plugin
  As a user, I can provide feedback so that I can improve the overall UI experience
  or report issues I encounter while using the system.  
  
  @UCXN-14143
  Scenario Outline: Send Feedback
    Given I am on the dashboard <USERNAME> and <PASSWORD>
    Then I should be able to send feedback

  @UCXN-14144
  Scenario: Feedback form should start empty whenever opened.
    Given I am on the dashboard
    When I open the feedback form after canceling a prior entry
    Then the feedback form should be empty
