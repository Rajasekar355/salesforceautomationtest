package web.salesforcedemoapp.testautomation.webtestclasses;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.utilities.BaseClass;
import com.utilities.ConfigFilesUtility;
import com.utilities.QFUtilities;
import com.google.common.base.Throwables;
import com.utilities.DriverInstance;
import org.testng.annotations.AfterTest;

import web.salesforcedemoapp.testautomation.webpageclasses.LoginScreen;
import web.salesforcedemoapp.testautomation.webpageclasses.HomeScreen;
import web.salesforcedemoapp.testautomation.webpageclasses.LeadsScreen;
import web.salesforcedemoapp.testautomation.webpageclasses.LeadCreationScreen;

public class TC_VerifyLeadCreation extends BaseClass { 

 	QFUtilities qf;
	private ConfigFilesUtility configFileObj;
	public boolean isElementDispalyed = false;
 	private String browserName = "chrome";
	private WebDriver driver;


	public TC_VerifyLeadCreation() throws Exception {
		configFileObj = new ConfigFilesUtility();
		qf = new QFUtilities();
		configFileObj.loadPropertyFile("tc_verifyleadcreation.properties");
	}

	@BeforeTest
	@Parameters("browser")
	public void launchBrowser(String browser) throws Exception {
		browserName = browser;
	}

	private void setup() {
		driver = launchBrowser(browserName, configFileObj);
	}

	private void loginScreenTest(int i) throws Exception {

		LoginScreen loginScreenObj = new LoginScreen();
		try { 
			printScreenReport("LoginScreen");
			qf.sendKeys(driver, loginScreenObj._username, configFileObj.getProperty("username1"  + i + "input"));
			printSuccessLogAndReport( "Entered username input: " + configFileObj.getProperty("username1"  + i + "input"));
			qf.sendKeys(driver, loginScreenObj._password, configFileObj.getProperty("password1"  + i + "input"));
			printSuccessLogAndReport( "Entered Password input: " + configFileObj.getProperty("password1"  + i + "input"));
			qf.clickAction(driver, loginScreenObj._login);
			printSuccessLogAndReport( "Clicked on Login");

		} catch (Exception e) {
			isElementDispalyed = false;
			printFailureLogAndReport(   "Element is not found " + Throwables.getStackTraceAsString(e));
		} finally {
			cleanupObject(loginScreenObj);
		}
	}

	private void homeScreenTest(int i) throws Exception {

		HomeScreen homeScreenObj = new HomeScreen();
		try { 
			printScreenReport("HomeScreen");
			qf.clickAction(driver, homeScreenObj._leads, configFileObj.getBooleanProperty("leads2"  + i + "click"));
			if(configFileObj.getBooleanProperty("leads2"  + i + "click"))
			printSuccessLogAndReport( "Clicked on Leads");

		} catch (Exception e) {
			isElementDispalyed = false;
			printFailureLogAndReport(   "Element is not found " + Throwables.getStackTraceAsString(e));
		} finally {
			cleanupObject(homeScreenObj);
		}
	}

	private void leadsScreenTest(int i) throws Exception {

		LeadsScreen leadsScreenObj = new LeadsScreen();
		try { 
			printScreenReport("LeadsScreen");
			qf.clickAction(driver, leadsScreenObj._new, configFileObj.getBooleanProperty("new3"  + i + "click"));
			if(configFileObj.getBooleanProperty("new3"  + i + "click"))
			printSuccessLogAndReport( "Clicked on New");

		} catch (Exception e) {
			isElementDispalyed = false;
			printFailureLogAndReport(   "Element is not found " + Throwables.getStackTraceAsString(e));
		} finally {
			cleanupObject(leadsScreenObj);
		}
	}

	private void leadCreationScreenTest(int i) throws Exception {

		LeadCreationScreen leadCreationScreenObj = new LeadCreationScreen();
		try { 
			printScreenReport("LeadCreationScreen");
			qf.sendKeys(driver, leadCreationScreenObj._lastName, configFileObj.getProperty("lastName4"  + i + "input")+ configFileObj.getProperty("random_number" + i));
			printSuccessLogAndReport( "Entered Last Name input: " + configFileObj.getProperty("lastName4"  + i + "input")+ configFileObj.getProperty("random_number" + i));
			qf.sendKeys(driver, leadCreationScreenObj._company, configFileObj.getProperty("company4"  + i + "input")+ configFileObj.getProperty("random_number" + i));
			printSuccessLogAndReport( "Entered Company input: " + configFileObj.getProperty("company4"  + i + "input")+ configFileObj.getProperty("random_number" + i));
			qf.clickAction(driver, leadCreationScreenObj._leadStatusOpenNurture, configFileObj.getBooleanProperty("leadStatusOpenNurture4"  + i + "click"));
			if(configFileObj.getBooleanProperty("leadStatusOpenNurture4"  + i + "click"))
			printSuccessLogAndReport( "Clicked on Lead Status, Open/Nurture");
			qf.clickAction(driver, leadCreationScreenObj._leadStatusdropdownValue);
			printSuccessLogAndReport( "Clicked on Lead Status dropdown Value");
			qf.clickAction(driver, leadCreationScreenObj._save, configFileObj.getBooleanProperty("save4"  + i + "click"));
			if(configFileObj.getBooleanProperty("save4"  + i + "click"))
			printSuccessLogAndReport( "Clicked on Save");

		} catch (Exception e) {
			isElementDispalyed = false;
			printFailureLogAndReport(   "Element is not found " + Throwables.getStackTraceAsString(e));
		} finally {
			cleanupObject(leadCreationScreenObj);
		}
	}

	@Test
	public void screensTest() throws Exception {
		for(int datasets = 1; datasets <= configFileObj.getIntProperty("datasetsLength"); datasets++) {
			isElementDispalyed = true;		 
			setup();			
			setTestcaseName(browserName,"TC_VerifyLeadCreation - " + configFileObj.getProperty("dataset" + (datasets)));
			if(isElementDispalyed) {loginScreenTest(datasets);}
			if(isElementDispalyed) {homeScreenTest(datasets);}
			if(isElementDispalyed) {leadsScreenTest(datasets);}
			if(isElementDispalyed) {leadCreationScreenTest(datasets);}
			tearDown();
		}	}

	
	public void tearDown() throws Exception {
		if(!DriverInstance.isMobilePlatform()) {
			driver.quit();
			driver=null;
		}
	}
	
	@AfterTest
	private void cleanup() {
		configFileObj =  null;
		qf = null;
	}

}