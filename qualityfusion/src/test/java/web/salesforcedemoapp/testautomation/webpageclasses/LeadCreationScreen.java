package web.salesforcedemoapp.testautomation.webpageclasses;

public class LeadCreationScreen { 

	public String _lastName = "//input[@name='lastName']";
	public String _company = "//input[@name='Company']";
	public String _leadStatusOpenNurture = "//label[contains(text(),'Lead Status')]/following::button[1]";
	public String _leadStatusdropdownValue = "//span[@title='Working']";
	public String _save = "(//button[contains(text(),'Save')])[2]";

 }