#gherkin
@smoke @sanity
Feature: redRail Train Ticket Booking

  Users should be able to efficiently use redRail services on the redbus.in website to book train tickets, check PNR status, and view live train statuses. There should also be an option to cancel for free with an instant refund.

  Background: 
    Given the user is on the redRail Train Ticket Booking page on redbus.in

  @bookTicket
  Scenario Outline: Book a train ticket
    When the user enters "<From>" as the departure station
    And the user enters "<To>" as the arrival station
    And the user selects "<Date>" as the travel date
    And the user clicks on the "Search Trains" button
    Then the available trains between "<From>" and "<To>" on "<Date>" should be displayed

    Examples:
      | From    | To      | Date          |
      | Mumbai  | Pune    | 14-May-2024   |
      | Chennai | Madurai | 20-Jun-2024   |

  @checkPNR
  Scenario: Check PNR status
    When the user navigates to the "Check PNR Status" section
    And the user enters a valid PNR number
    And the user clicks on the "Submit" button
    Then the system should display the current status of the train ticket associated with the PNR

  @liveStatus
  Scenario Outline: Check live status of a train
    When the user navigates to the "Live Train Status" section
    And the user enters "<Train_Number>" in the train number field
    And the user clicks on the "Check Status" button
    Then the system should display the live status for train "<Train_Number>"

    Examples:
      | Train_Number |
      | 12345        |
      | 67890        |

  @cancelTicket
  Scenario Outline: Option for free cancellation
    Given the user has already booked a ticket from "<From>" to "<To>" on "<Date>"
    And the user navigates to their current bookings page
    When the user selects the booking and clicks the "Cancel Ticket" button
    Then the ticket should be cancelled without any cancellation fee
    And the user should receive a confirmation of the cancellation and instant refund details

    Examples:
      | From    | To      | Date          |
      | Delhi   | Agra    | 15-Jul-2024   |
      | Kolkata | Siliguri| 01-Aug-2024   |
