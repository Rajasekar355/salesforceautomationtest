import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PnrStatusPage {
    WebDriver driver;

    @FindBy(id = "pnr_input")
    WebElement pnrInput;

    @FindBy(id = "submit_button")
    WebElement submitButton;

    public PnrStatusPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void enterPnr(String pnr) {
        pnrInput.sendKeys(pnr);
    }

    public void clickSubmit() {
        submitButton.click();
    }
}