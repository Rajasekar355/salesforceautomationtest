import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RedRailBookingPage {
    WebDriver driver;

    @FindBy(id = "departure_station")
    WebElement departureStation;

    @FindBy(id = "arrival_station")
    WebElement arrivalStation;

    @FindBy(id = "travel_date")
    WebElement travelDate;

    @FindBy(id = "search_trains_button")
    WebElement searchTrainsButton;

    public RedRailBookingPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void enterDepartureStation(String from){
        departureStation.sendKeys(from);
    }

    public void enterArrivalStation(String to){
        arrivalStation.sendKeys(to);
    }

    public void selectTravelDate(String date){
        travelDate.sendKeys(date);
    }

    public void clickSearchTrains(){
        searchTrainsButton.click();
    }
}