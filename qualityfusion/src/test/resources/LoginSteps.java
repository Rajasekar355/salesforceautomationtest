import io.cucumber.java.en.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import static org.junit.Assert.*;

public class LoginSteps {
    private WebDriver driver = new ChromeDriver();
    private LoginPage loginPage = new LoginPage(driver);

    @Given("a user navigates to the login page")
    public void a_user_navigates_to_the_login_page() {
        driver.navigate().to("http://example.com/login");
    }

    @When("the user enters their valid credentials")
    public void the_user_enters_their_valid_credentials() {
        loginPage.login("validUser", "validPass");
    }

    @When("the user enters the {string} and {string}")
    public void the_user_enters_the_username_and_password(String username, String password) {
        loginPage.login(username, password);
    }

    @And("clicks on the login button")
    public void clicks_on_the_login_button() {
        loginPage.clickLoginButton();
    }

    @Then("the user is redirected to their dashboard")
    public void the_user_is_redirected_to_their_dashboard() {
        assertTrue(driver.getCurrentUrl().contains("/dashboard"));
    }

    @Then("the user should see an error message {string}")
    public void the_user_should_see_an_error_message(String message) {
        assertEquals(message, loginPage.getErrorMessage());
    }

    // Additional steps for field validations etc., could be implemented similarly.
}

@After
public void tearDown() {
    driver.quit();
}