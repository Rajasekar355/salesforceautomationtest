package com.utilities;

public class Queries {
	
	
	public static String formMasterDataQuery(String masterids) {
		return "select name as siteName,store_brand as storeBrand,corporate_brand as corpBrand,fuel_brand as fuelBrand,address_1 as address1,address_2 as address2,city as city, region as state,postal_code as postcode,latitude as latitude,longitude as longitude,location_id as masterSiteId from flip.location_master where location_id in (" + masterids  + ")  ORDER BY FIELD(location_id, " + masterids + ")";
	}

	
	public static String formFleetNetSites(String materSiteIds, String siteIds) {
		
		return "select source_identifier as siteId from flip.location_master_xref xref , flip.location_fn fn where "
				+ " xref.location_id_match =fn.location_id "
				+ "AND xref.location_id in (" + materSiteIds + ") and source_identifier in ("+ siteIds +")"
				+ " ORDER BY FIELD(source_identifier, " + siteIds + ")";
	}
	
public static String formFleetNetSites1(String materSiteIds, String siteIds) {
		/*
		return "select fn.source_identifier as siteId from flip.location_master_xref xref , flip.location_fn fn where "
				+ " xref.location_id_match =fn.location_id "
				+ " AND xref.location_id in (" + materSiteIds + ") and fn.source_identifier in ("+ siteIds +")"
				+ "union select mc.source_identifier as siteId from flip.location_master_xref xref ,flip.location_mc mc where  xref.location_id_match =mc.location_id"
				+" AND xref.location_id in("+ materSiteIds +") and mc.source_identifier in ("+ siteIds +")"
				+ "union select cfn.source_identifier as siteId from flip.location_master_xref xref ,flip.location_cfn cfn where  xref.location_id_match =cfn.location_id"
				+" AND xref.location_id in("+ materSiteIds +") and cfn.source_identifier in ("+ siteIds +")"
				+ " ORDER BY FIELD(siteId, " + siteIds + ")";*/
		
		return	"select fn.source_identifier as siteId,fn.store_brand as storeBrand,fn.fuel_brand as fuelBrand,"
				+ " DATE_FORMAT(SUBSTRING_INDEX(SUBSTRING_INDEX(fn.attributes, '\"last-trx-date\":\"', -1), '\"', 1),'%m-%d-%Y') "
				+ " as lastTrxDateFormatted "
				+ " from flip.location_master_xref xref , "
		 + " flip.location_fn fn,flip.location_master ms "
		+ "  where  xref.location_id_match =fn.location_id "
		+ "  and ms.location_id = xref.location_id "
		+ "  AND xref.location_id in ("+ materSiteIds +") "
		 + " and fn.source_identifier in ("+ siteIds +") ";
		
		
	}

public static String formFleetNetReports(String materSiteIds, String siteIds) {
	
	return "select fn.attributes from flip.location_fn fn,flip.location_master mast,flip.location_master_xref xref where xref.location_id_match =fn.location_id" 
			+" and mast.location_id = xref.location_id"
			+" and status = 1 and fn.source_identifier in (" + siteIds + ")"
		   + "AND mast.location_id="+materSiteIds+"";
}

public static String nearByDB(String materSiteIds, String siteIds) {
	

	return " select ms.location_id,ms.status,fn.source_identifier,fn.name,fn.status_id,fn.store_brand,fn.fuel_brand, "
			+ " fn.address_1,fn.city,fn.region,fn.postal_code,fn.latitude,fn.longitude, "
			+ " (SUBSTRING_INDEX(SUBSTRING_INDEX(fn.attributes, '\"fuel-flag\":\"', -1), '\"', 1)) "
			+ " as fuelflag, (SUBSTRING_INDEX(SUBSTRING_INDEX(fn.attributes, '\"listing-status\":\"', -1), '\"', 1)) as listing_status "
			+ " from flip.location_master_xref xref ,"
			+ " flip.location_fn fn,flip.location_master ms"
			+ " where  xref.location_id_match =fn.location_id"
			+ " and ms.location_id = xref.location_id"
			+ " AND xref.location_id in "
			+ " ("+ materSiteIds +") "
		    + " and fn.source_identifier in ("+ siteIds +") ";
	
}

public static String queryLatLang() {
	
	return " select * from flip.location_master where (latitude is NOT null or longitude is NOT null) limit 1"; 
	
}

public static String queryLatLanggood() {
	
	return " select * from flip.location_master where (latitude is null or longitude is null) and status =1 limit 1;"; 
	
}
public static String queryLatLangbad() {
	
	return " select * from flip.location_master where (latitude is null or longitude is null) and status =-1 limit 1"; 
	
}
} 
