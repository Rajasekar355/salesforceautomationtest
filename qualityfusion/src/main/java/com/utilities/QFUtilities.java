package com.utilities;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.AppiumDriver;

public class QFUtilities {

	// InputText method -- you can customize by using Actions
	public void sendKeys(WebDriver driver, String xpath, String inputValue) {
		By locator = getLocator(xpath);
		
			WebElement element = waitForElement(driver, locator);
			try {
				element.click();
			} catch (Exception e) {
				// OK
			}
			inputClear(element);
			element.sendKeys(inputValue);
	
	}

	public void clickAction(WebDriver driver, String xpath) {
		By locator = getLocator(xpath);
		try {
			fluentWait(driver, ExpectedConditions.elementToBeClickable(locator), 2);
			click(driver, locator);
		} catch (org.openqa.selenium.NoSuchElementException ex) {
			fluentWait(driver, ExpectedConditions.elementToBeClickable(locator), 2);
			javascriptClick(driver, locator);
		} catch (org.openqa.selenium.StaleElementReferenceException ex) {
			fluentWait(driver, ExpectedConditions.elementToBeClickable(locator), 2);
			javascriptClick(driver, locator);
		} catch (org.openqa.selenium.ElementNotInteractableException ex) {
			fluentWait(driver, ExpectedConditions.elementToBeClickable(locator), 2);
			javascriptClick(driver, locator);
		} catch (Exception e) {

			// for hidden element
			WebElement tmpElement = driver.findElement(locator);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", tmpElement);
		}
	}

	public void clickAction(WebDriver driver, String xpath, boolean isClick) {

		if (isClick) {
			clickAction(driver, xpath);
		}
	}

	private void click(WebDriver driver, By locator) {
		waitForElement(driver, locator);
		Actions actions = new Actions(driver);
		WebElement hoverOption = driver.findElement(locator);
		actions.moveToElement(hoverOption).build().perform();
		hoverOption.click();
	}

	private void javascriptClick(WebDriver driver, By locator) {
		WebElement element = waitForElement(driver, locator);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		// executor.executeScript("arguments[0].scrollIntoView(true);", element);
		executor.executeScript("arguments[0].click();", element);
	}

	public String selectOption(WebDriver driver, String xpath, String optionValue) {
		By locator = getLocator(xpath);
		waitForElement(driver, locator);
		// driver.findElement(locator).click();
		new Select(driver.findElement(locator)).selectByVisibleText(optionValue.trim());
		// driver.findElement(locator).click();
		String actualText = new Select(driver.findElement(locator)).getFirstSelectedOption().getText();
		return actualText;
	}

	public String validateSelectOption(WebDriver driver, String xpath, String expectedText) {
		By locator = getLocator(xpath);
		waitForElement(driver, locator);
		Select select = new Select(driver.findElement(locator));
		String actualText = select.getFirstSelectedOption().getText();
		String actualTxt = "";
		if (expectedText.contains(actualText) || actualText.contains(expectedText)) {
			actualTxt = "Pass:Validation: Expected selected option: " + expectedText + "</br> Actual selected option: "
					+ actualText;
		} else {
			actualTxt = "Fail: Expected: " + expectedText + "<br/>";
			actualTxt = actualTxt + "Actual: " + actualText + "<br/>";
			actualTxt = actualTxt + "Actual and Expected text are not equal. Expected is: " + expectedText;
		}

		return actualTxt;

	}

	public void mouseHover(WebDriver driver, String xpath, boolean isClick) {
		By locator = getLocator(xpath);
		waitForElement(driver, locator);
		Actions actions = new Actions(driver);
		WebElement hoverOption = driver.findElement(locator);
		actions.moveToElement(hoverOption).build().perform();
		if (isClick)
			hoverOption.click();

	}

	// Validate InputText method -- you can customize by using Actions
	public String validateInput(WebDriver driver, String xpath, String expectedText) {
		By locator = getLocator(xpath);
		waitForElement(driver, locator);
		WebElement element = driver.findElement(locator);
		String actualText = element.getAttribute("value");
		String actualTxt = "";
		if (expectedText.contains(actualText) || actualText.contains(expectedText)) {
			actualTxt = "Pass: Validation:</br> Expected: " + expectedText + "</br> Actual: " + actualText;
		} else {
			actualTxt = "Fail: Expected: " + expectedText + "<br/>";
			actualTxt = actualTxt + "Actual: " + actualText + "<br/>";
			actualTxt = actualTxt + "Actual and Expected text are not equal. Expected is: " + expectedText;
		}

		return actualTxt;
	}

	// Validate InputText method -- you can customize by using Actions
	public String validateLabel(WebDriver driver, String xpath, String validateMsg) {
		By locator = getLocator(xpath);
		waitForElement(driver, locator);
		WebElement element = driver.findElement(locator);
		return element.getText();
	}

	// Validate InputText method -- you can customize by using Actions
	public String validateAndClickAction(WebDriver driver, String xpath, String expectedText, boolean isClick) {
		By locator = getLocator(xpath);
		waitForElement(driver, locator);
		WebElement element = driver.findElement(locator);
		String actualText = element.getText();
		if (actualText.isEmpty()) {
			actualText = element.getAttribute("title");
		}
		
		//expectedText = org.apache.commons.text.StringEscapeUtils.unescapeJava(org.apache.commons.text.StringEscapeUtils.unescapeJava(expectedText));
		String actualTxt = "";
		if (expectedText.equals(actualText)) {
			actualTxt = "Pass: ";
		} else {
			actualTxt = "Fail: ";
		}

		actualTxt = actualTxt + "Validation:</br>" + "Expected => " + expectedText + "<br/> Actual => " + actualText;

		if (isClick) {
			element.click();
			actualTxt = actualTxt + "</br>Clicked on " + expectedText;
		}

		return actualTxt;
	}

	// Validate InputText method -- you can customize by using Actions
	public String validateInputButton(WebDriver driver, String xpath, String expectedText, boolean isClick) {
		By locator = getLocator(xpath);
		waitForElement(driver, locator);
		WebElement element = driver.findElement(locator);
		String actualText = element.getAttribute("value");

		String actualTxt = "";
		if (expectedText.equalsIgnoreCase(actualText)) {
			actualTxt = "Pass: Validation: Expected: " + expectedText + "</br>" + "Actual: " + actualText;
		} else {
			actualTxt = "Fail: Expected: " + expectedText + "<br/>";
			actualTxt = actualTxt + "Actual: " + actualText + "<br/>";
			actualTxt = actualTxt + "Actual and Expected text are not equal. Expected is: " + expectedText;
		}

		if (isClick)
			element.click();

		return actualTxt;
	}

	// Validate InputText method -- you can customize by using Actions
	public String isElementDisplayed(WebDriver driver, String xpath, String fieldName, boolean isClick) {
		By locator = getLocator(xpath);
		try {
			waitForElement(driver, locator);
			WebElement element = driver.findElement(locator);

			if (fieldName.isEmpty()) {
				try {

					fieldName = element.getText();
					if (fieldName == null || fieldName.isEmpty()) {
						fieldName = element.getAttribute("value");
					}

					if (fieldName == null || fieldName.isEmpty()) {
						fieldName = element.getAttribute("title");
					}
				} catch (Exception e) {
					fieldName = element.getAttribute("value");
				}
			}

			if (element.isDisplayed()) {
				String text = "Pass: Validation:</br>" + fieldName + " is displayed";

				try {

					text = text + Utilities.takeCanvasScreenshot(element, fieldName);
					if (isClick) {
						element.click();
						text = text + "<br/>" + fieldName + " is Clicked";
					}
				} catch (Exception e) {
					System.err.println("==================");
				}
				return text;
			} else
				return "Fail: Need Investigation - " + fieldName + " Expected element is not displayed.  Please check "
						+ xpath;
		} catch (Exception e) {
			return "Fail: Need Catch Investigation - " + fieldName + "Expected element is not displayed.  Please check "
					+ xpath + " : " + e.getLocalizedMessage();
		}
	}

	public String isElementNotDisplayed(WebDriver driver, String xpath, String fieldName, boolean isClick) {
		By locator = getLocator(xpath);
		try {
			waitForElement(driver, locator);
			WebElement element = driver.findElement(locator);
			if (element.isDisplayed()) {
				String text = "Fail: Validation:</br>" + fieldName + " is displayed";
				try {
					text = text + Utilities.takeCanvasScreenshot(element, fieldName);
				} catch (Exception e) {
					System.err.println("==================");
				}
				return text;
			} else
				return "Pass: Need Investigation - " + fieldName + " Expected element is  displayed.  Please check "
						+ xpath;
		} catch (NoSuchElementException e) {
			return "Fail: Need Catch Investigation - " + fieldName + e.getLocalizedMessage();
		} catch (Exception e) {
			return "Fail: Need Catch Investigation - " + fieldName + e.getLocalizedMessage();
		}
	}

	// window
	String parentHandle = "";

	public void windowHandle(WebDriver webDriver) {
		parentHandle = webDriver.getWindowHandle();
		Set<String> handles = webDriver.getWindowHandles();
		for (String windowHandles : handles) {
			System.out.println(windowHandles);
			webDriver.switchTo().window(windowHandles);
		}
	}

	public void switchToParentWindow(WebDriver webDriver) {
		if (parentHandle != null && !parentHandle.isEmpty()) {
			webDriver.switchTo().window(parentHandle);
		}
	}

	public void switchToIframe(WebDriver driver, String xpath) {

		try {
			boolean isElementFound = false;
			for (int j = 0; j < 5; j++) {
				Thread.sleep(2000);
				for (int framePosition = 0; framePosition <= 5; framePosition++) {
					try {
						driver.switchTo().defaultContent();
						driver.switchTo().frame(framePosition);
						try {
							WebElement ele = driver.findElement(getLocator(xpath));
							if (ele.isDisplayed()) {
								waitForElement(driver, getLocator(xpath));
								isElementFound = true;
								System.out.println(framePosition);
								break;
							}
						} catch (Exception e) {
							try {
								List<WebElement> elements1 = driver.findElements(By.tagName("frame"));
								List<WebElement> iframeElements = null;
								if (elements1 != null && elements1.size() > 0) {
									for (int m = 0; m <= elements1.size(); m++) {
										try {
											driver.switchTo().frame(m);
											iframeElements = driver.findElements(By.tagName("iframe"));
											WebElement ele = driver.findElement(getLocator(xpath));
											if (ele.isDisplayed()) {
												waitForElement(driver, getLocator(xpath));
												isElementFound = true;
												System.out.println(framePosition);
												break;
											}
										} catch (Exception e1) {
											if (iframeElements != null && iframeElements.size() > 0) {
												for (int m1 = 0; m1 <= iframeElements.size(); m1++) {
													driver.switchTo().frame(m1);
													WebElement ele = driver.findElement(getLocator(xpath));
													if (ele.isDisplayed()) {
														waitForElement(driver, getLocator(xpath));
														isElementFound = true;
														System.out.println(framePosition);
														break;
													}
												}
											}
											if (!isElementFound) {
												driver.switchTo().parentFrame();
											} else {
												break;
											}
										}
									}

									if (isElementFound) {
										break;
									}
								}
							} catch (Exception e2) {
								// TODO: handle exception
							}
							try {
								List<WebElement> elements1 = driver.findElements(By.tagName("iframe"));

								if (elements1 != null && elements1.size() > 0) {

									for (int m = 0; m <= elements1.size(); m++) {

										try {

											driver.switchTo().frame(m);

											WebElement ele = driver.findElement(getLocator(xpath));

											if (ele.isDisplayed()) {

												waitForElement(driver, getLocator(xpath));

												isElementFound = true;

												System.out.println(framePosition);

												break;

											}

										} catch (Exception e1) {

											driver.switchTo().parentFrame();

										}

										// driver.switchTo().defaultContent();

									}

									if (isElementFound) {

										break;

									}

								}

							} catch (Exception e3) {

								// TODO: handle exception

							}

						}

					} catch (Exception e) {

						driver.switchTo().defaultContent();

					}

				}

				if (isElementFound || j == 4) {

					break;

				}

			}

		} catch (Exception e) {

			driver.switchTo().defaultContent();

		}

	}

	public void selectiOption(WebDriver driver, String xpath, String optionValue) {
		By locator = getLocator(xpath);
		waitForElement(driver, locator);
		driver.findElement(locator).click();
		{
			WebElement dropdown = driver.findElement(locator);
			dropdown.findElement(By.xpath("//option[. = '" + optionValue + "']")).click();
		}
		driver.findElement(locator).click();
	}

	public void alertHandle(WebDriver driver, boolean isAlertAccept) {
		try {
			Alert alert = driver.switchTo().alert();
			Thread.sleep(5000);
			if (isAlertAccept) {
				alert.accept();
			} else {
				alert.dismiss();
			}
		} catch (Exception e) {

		}
	}

	public boolean isElementDisplayed(WebDriver driver, String xpath) {
		try {
			By locator = getLocator(xpath);
			WebElement element = waitForElement(driver, locator);
			return element.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}
	
	public boolean optionalElement(WebDriver driver, String xpath) {
		try {
			By locator = getLocator(xpath);
			WebElement element = driver.findElement(locator);
			return element.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	private By getLocator(String xpath) {
		if (xpath.startsWith("/") || xpath.startsWith("(")) {
			return By.xpath(xpath);
		} else {
			return By.cssSelector(xpath);
		}
	}

	public void scrollUp(WebDriver driver, String xpath) {

		boolean isDisplayed = false;
		for (int i = 0; i < 10; i++) {
			try {
				JavascriptExecutor js = ((JavascriptExecutor) driver);
				js.executeScript("window.scrollTo(0,-100");
				WebElement element = driver.findElement(getLocator(xpath));
				if (element.isDisplayed()) {
					isDisplayed = true;
					break;
				}
			} catch (Exception e) {

			}
		}

		if (!isDisplayed) {
			Actions a = new Actions(driver);
			a.sendKeys(Keys.PAGE_UP).build().perform();
		}
	}

	public void scrollDown(WebDriver driver, String xpath) {

		boolean isDisplayed = false;
		for (int i = 0; i < 10; i++) {
			try {
				JavascriptExecutor js = ((JavascriptExecutor) driver);
				js.executeScript("window.scrollTo(0,300");
				WebElement element = driver.findElement(getLocator(xpath));
				if (element.isDisplayed()) {
					isDisplayed = true;
					break;
				}
			} catch (Exception e) {
				Actions a = new Actions(driver);
				a.sendKeys(Keys.PAGE_DOWN).build().perform();
			}
		}

		if (!isDisplayed) {
			Actions a = new Actions(driver);
			a.sendKeys(Keys.PAGE_DOWN).build().perform();
		}

	}

	public void fluentWait(WebDriver driver, final ExpectedCondition<?> expectedCondition, int timeout) {
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(timeout))
				.pollingEvery(Duration.ofSeconds(10))
				.ignoring(NoSuchElementException.class)
				.ignoring(StaleElementReferenceException.class);
		wait.until(expectedCondition);
	}

	public void wait(WebDriver driver, String xpath) {
		try {
			fluentWait(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)), 10);
		} catch (Exception e) {
			//OK
		}
	}

	public void enter(WebDriver driver, String xpath) {
		driver.findElement(getLocator(xpath)).sendKeys(Keys.ENTER);
	}

	public void tab(WebDriver driver, String xpath) {
		driver.findElement(getLocator(xpath)).sendKeys(Keys.TAB);
	}

	public WebElement waitForElement(WebDriver driver, WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));
		wait.pollingEvery(Duration.ofSeconds(10));
		return wait.until(ExpectedConditions.visibilityOf(element));
	}

	public WebElement waitForElement(WebDriver driver, By locator) {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));
		return wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
	}

	public WebElement waitForElementClickable(WebDriver driver, By locator) {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));
		return wait.until(ExpectedConditions.elementToBeClickable(locator));
	}

	public void inputClear(WebElement element) {
		element.clear();
		try {
			while (!element.getAttribute("value").equals("") && element.getAttribute("type").equals("text")) {
				element.sendKeys(Keys.BACK_SPACE);
			}

		} catch (Exception e) {
			System.out.println("Not editable input" + e.getLocalizedMessage());
		}

	}

	public void malinatorPublic() {

	}

	public boolean skipForMobileExecution(RemoteWebDriver driver, String xpath) {
		if(driver instanceof AppiumDriver) {
			return false;
		}
		return true;
	}

	public boolean skipForWebExecution(RemoteWebDriver driver, String _clearfix) {
		if(driver instanceof AppiumDriver) {
			return true;
		}
		return false;
	}
	
	/*
	 * Removing spaces and unknown chars from string
	 */

	public static String removeUnWantedChars(String value) {
		try {
			String val = value.replaceAll(" ", "");
			val = val.replaceAll("[^a-z_A-Z0-9]", "");
			return val;
		} catch (Exception e) {
			return value;
		}
	}

	public void waitForElement(RemoteWebDriver driver, String xpath, int timeInSecs) {
		fluentWait(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)), timeInSecs);
	}


	
	public static void saveProperties(String projectName, String propKey, String content) {
		String path = "." + File.separator + "ConfigFiles" + File.separator + projectName + ".properties";
		File file = new File(path);
		try {
			file.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// Checking If The File Exists At The Specified Location Or Not
		Path filePathObj = Paths.get(path);
		boolean fileExists = Files.exists(filePathObj);
		if(fileExists) {
			try {
				List<String> fileContent = new ArrayList<>(Files.readAllLines(file.toPath(), StandardCharsets.UTF_8));
				boolean isOverwrited = false;
				for (int i = 0; i < fileContent.size(); i++) {
					if (fileContent.get(i).contains(propKey + " = ")) {
						fileContent.set(i, propKey + " = " + content);
						isOverwrited = true;
						break;
					}
				}
				if(isOverwrited) {
					Files.write(file.toPath(), fileContent, StandardCharsets.UTF_8);
				} else {
					content = "\n" + propKey + " = " + content;
					Files.write(filePathObj, content.getBytes(), StandardOpenOption.APPEND);
				}

			} catch (IOException ioExceptionObj) {
				System.out.println("Problem Occured While Writing To The File= " + ioExceptionObj.getMessage());
			}
		} else {
			System.out.println("File Not Present! Please Check!");
		}     
	}

}
