package com.utilities;

import java.util.HashMap;
import java.util.Map;

import io.appium.java_client.AppiumDriver;

public class DriverInstance {
	
	private static Map<Integer, AppiumDriver> appiumDriverMap = new HashMap<Integer, AppiumDriver>();

	private static boolean isJobStatus;
	private static boolean isMobilePlatform;

	@SuppressWarnings("deprecation")
	public static AppiumDriver getDriver() {
		return appiumDriverMap.get((int)Thread.currentThread().getId());
	}

	public static boolean isMobilePlatform() {
		return isMobilePlatform;
	}

	public static void setMobilePlatform(boolean mobilePlatform) {
		isMobilePlatform = mobilePlatform;
	}

	@SuppressWarnings("deprecation")
	public static void setDriver(AppiumDriver driver) {
		 appiumDriverMap.put((int)Thread.currentThread().getId(), driver);
	}
	
	

	public static void setJobStatus(boolean jobStatus) {
		isJobStatus = jobStatus;
	}

	public static boolean getJobStatus() {
		return isJobStatus;
	}
}
