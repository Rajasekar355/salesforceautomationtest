package com.utilities;

import static io.restassured.RestAssured.given;

import org.apache.commons.text.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.customcode.QFCustomizedCode;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
@SuppressWarnings("unused")
public class RestUtils {



	private static ConfigFilesUtility configFileObj;
	private static Integer reqType;
	private static Integer bodyType;
	private static String rawBody;



	
	public static String getRequest(ConfigFilesUtility con,Object object, int i) {


		try {
			// Test data
			
			String data  = con.getProperty("PrimaryInfo");
			JSONObject jsonObject = new JSONObject(data);
			String testCaseName = jsonObject.getString("testcase_name");
			
			Class<?> c = object.getClass();
			String urlParams = (String) c.getField("urlParams" + i).get(object);
			String headers = (String) c.getField("headers" + i).get(object);
			String body = (String) c.getField("body" + i).get(object);
			String authenticationData = (String) c.getField("authenticationData" + i).get(object);
			String apiName = (String) c.getField("apiName" + i).get(object);
			String datasetResources = (String) c.getField("datasetResources" + i).get(object);
			reqType = (Integer) c.getField("requestType" + i).get(object);
			bodyType = (Integer) c.getField("bodyType" + i).get(object);
			String header = (String) c.getField("datasetHeader" + i).get(object);
			String Url = getFinalData(jsonObject.optString("project_url") + datasetResources);
			
			RequestSpecification requestSpec = given();

			//Add Headers
			addHeaders(requestSpec, headers);

			//Add params or Query Params
			addParmsOrQueryParams(requestSpec, urlParams, apiName);

			Response response = requestSpec.when()
					.get(Url)
					.then()
					.extract().response();
			
			QFCustomizedCode.customCode(apiName, body, urlParams, response.asString(),"pepdemo", configFileObj);

			return response.asString();
		} catch (Exception e) {
			return null;
		}

	}


	public static String postRequest(Object object, int i) {

		try {
			// Test data
			Class<?> c = object.getClass();
			String urlParams = (String) c.getField("urlParams" + i).get(object);
			String headers = (String) c.getField("headers" + i).get(object);
			String body = (String) c.getField("body" + i).get(object);
			String authenticationData = (String) c.getField("authenticationData" + i).get(object);
			String apiName = (String) c.getField("apiName" + i).get(object);
			String datasetResources = (String) c.getField("datasetResources" + i).get(object);
			reqType = (Integer) c.getField("requestType" + i).get(object);
			bodyType = (Integer) c.getField("bodyType" + i).get(object);
			String header = (String) c.getField("datasetHeader" + i).get(object);
			String formData = "";
			String formUrlEncodedData = "";
			JSONObject inputBody = new JSONObject(body);

			RequestSpecification requestSpec = given();

			//Add Headers
			addHeaders(requestSpec, headers);

			//Add params or Query Params
			addParmsOrQueryParams(requestSpec, urlParams,apiName);
			
			//Add Form queryParms or FormUrl encoded
			addFormUrlOrFormUrlEncoded(requestSpec, urlParams, formUrlEncodedData, formData, new JSONObject(inputBody));
			
			addRawBody(inputBody);

			Response response = requestSpec.when()
					.get(datasetResources)
					.then()
					.extract().response();

			return response.asString();
		} catch (Exception e) {
			return null;
		}

	}





	public static void addHeaders(RequestSpecification requestSpec, String headers) {

		JSONArray headersJsonArray = new JSONArray(headers);
		for (int i = 0; i < headersJsonArray.length(); i++) {
			JSONObject headerObj = headersJsonArray.getJSONObject(i);
			String headerkey = getFinalData(headerObj.getString("header_key"));
			String headerValue = getFinalData(headerObj.getString("header_value"));
			//reportCreation("info", headerkey + " : "+ headerValue);
			requestSpec.header(headerkey, headerValue);
		}

	}

	public static void addFormUrlOrFormUrlEncoded(RequestSpecification requestSpec, String urlParams, String formurlEncodedData, String formData, JSONObject body) {

		JSONArray bodyArray = null;
		if (bodyType == 1 || bodyType == 2) { // form-data or x-www-form-urlencoded
			if(bodyType == 2) {
				bodyArray = new JSONArray(formurlEncodedData);
			}
			if(bodyType == 1) {
				bodyArray = body.optJSONArray(formData);
			}
			for (int i = 0; i < bodyArray.length(); i++) {
				JSONObject bodyObj = bodyArray.getJSONObject(i);

				String key = getFinalData(bodyObj.optString("key")).replaceAll("\n", "");
				String value =getFinalData(bodyObj.optString("value")).replaceAll("\n", "");
				//reportCreation("info", key + " : " + value);
				requestSpec.formParam(key, value);
			}
		} 
	}


	public static void addRawBody(JSONObject body) {
		if (bodyType == 3) { // raw data
			rawBody = getFinalData(body.optString("raw_text"));
			System.out.println("raw body :  " + StringEscapeUtils.unescapeJava(rawBody));
			if (body.length() > 0) {
				//extentHeaderLog( "Input Body");
				//reportCreation("info", "body  :  " + rawBody.toString());
				System.out.println("body :  " + StringEscapeUtils.escapeHtml4(rawBody.toString()));
			}
		}
	}



	public static void addParmsOrQueryParams(RequestSpecification requestSpec, String urlParams, String apiName) {
		JSONArray parameters = new JSONArray(urlParams);
		for (int i = 0; i < parameters.length(); i++) {
			JSONObject parametersObj = parameters.getJSONObject(i);
			String key = getFinalData(parametersObj.getString("param_key")).replaceAll("\n", "");
			String value = QFCustomizedCode.updateApiInputParams(apiName,key);
			if(value == null || value.isEmpty())
				value = getFinalData(parametersObj.getString("param_value")).replaceAll("\n", "");
			if (reqType > 1) {
				//reportCreation("info", key + " : "+ value);
				requestSpec.queryParam(key, value);
			} else {
				//reportCreation("info", key + " : "+ value);
				requestSpec.param(key, value);
			}
		}
	}



	private static String getFinalData(String splitData) {
		String returnData = splitData;
		try {

			if (splitData.contains("$") && splitData.contains("#")) {

				String[] data = splitData.split("\\$");
				for (int i = 0; i < data.length; i++) {
					if (data[i].startsWith("#") && data[i].endsWith("#")) {
						System.out.println(data[i]);
						String replacement = data[i].replaceAll("#", "");
						if(configFileObj == null) 
							return "";
						String finalConfigData = configFileObj.getProperty(replacement);
						returnData = returnData.replaceAll("\\$", "").replaceAll("#", "").replace(replacement,
								finalConfigData);
					}
				}
			}
		}catch (Exception e) {
			return splitData + "<br><br>Please check ordering the testcases and given parameters is valid or not";
		}
		return returnData;
	}


}
