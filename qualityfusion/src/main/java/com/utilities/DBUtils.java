package com.utilities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.restassured.services.APIService;


public class DBUtils {

	static final String DB_URL = "jdbc:mysql://<hostname>/servicename";
	static final String USERNAME = "username";
	static final String PASSWORD = "password";
	static Connection con;
	
	

	public static Connection createDbConnection() {
		
		try {
			if (con == null)
				con = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return con;
	}

	public static ArrayList<LinkedHashMap<String, String>> getList(String query) {

		con = createDbConnection();
		try {
			ArrayList<LinkedHashMap<String, String>> data = new ArrayList<LinkedHashMap<String, String>>();

			if (con != null) {
				Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
				ResultSet rs = stmt.executeQuery(query);
				if (rs != null && rs.next()) {
					java.sql.ResultSetMetaData rsmd = rs.getMetaData();
					rs.last();
					int rowCount = rs.getRow();
					rs.first();
					int columnCount = rsmd.getColumnCount();

					for (int i = 1; i <= rowCount; i++) {
						LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
						for (int j = 1; j <= columnCount; j++) {

							String key = rsmd.getColumnLabel(j);
							if (key == null)
								key = rsmd.getColumnName(j);
							String value = rs.getString(j);
							map.put(key, value);
						}

						data.add(map);
						rs.next();
					}

				}

				stmt.close();
				rs.close();

			} else {
				System.out.println("Connection not established.");
			}

			return data;

		} catch (Exception e) {
			return null;
		}

	}

	public static void dbValidateFromResponse(String query, List<Object> list) {

		ArrayList<LinkedHashMap<String, String>> data = DBUtils.getList(query);
		if (data == null)
			return;

		for (int j = 0; j < data.size(); j++) {
			LinkedHashMap<String, String> map1 = data.get(j);
			@SuppressWarnings("unchecked")
			Map<String, Object> map2 = (Map<String, Object>) list.get(j);
			for (Map.Entry<String, String> entry : map1.entrySet()) {
				String key = entry.getKey();
				String resValue = "" + map1.get(key);
				String dbValue = "" + map2.get(key);
				APIService.reportCreation("info", "DB " + key + " Value : " + dbValue);
				APIService.reportCreation("info", " API Response " + key + " Value : " + resValue);
				if (resValue.equals(dbValue)) {
					System.out.println((key + ":" + dbValue + "  DB value"));
					System.out.println((key + ":" + resValue + "  API value"));
					System.out.println((key + ":" + resValue + " value is matched with DB"));
					APIService.reportCreation("pass", key + ":" + resValue + " value is matched with DB");
				} else {

					System.out.println(key + ":" + resValue + " value is not matched with DB");
					APIService.reportCreation("fail", key + ":" + resValue + " value is not matched with DB");
				}
			}
		}

	}



}

