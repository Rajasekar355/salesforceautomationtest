package com.utilities;

import static org.testng.AssertJUnit.assertTrue;

import java.awt.Toolkit;
import java.io.File;
import java.lang.reflect.Method;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.winium.DesktopOptions;
import org.openqa.selenium.winium.WiniumDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;

import com.aventstack.extentreports.Status;
import com.configurations.GlobalData;
import com.customcode.QFCustomizedCode;
import com.epam.healenium.SelfHealingDriver;
import com.epam.reportportal.testng.ReportPortalTestNGListener;
import com.extentreport.ExtTest;
import com.google.common.base.Throwables;

import groovy.json.StringEscapeUtils;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.AutomationName;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.github.bonigarcia.wdm.WebDriverManager;
@Listeners({ReportPortalTestNGListener.class})
public class BaseClass {
	
	public WiniumDriver winiumDriver;
	private static boolean isElementDispalyed;
	private static AppiumDriverLocalService service;
	private String driversPath = System.getProperty("user.dir") + File.separator + "resources" + File.separator;
	private String chromeDriverPath = driversPath + "chromedriver.exe";
	private String geckoFireFoxDriverPath = driversPath + "geckodriver.exe";

//	public AppiumDriver<WebElement> appiumDriver;
	public String text = "";
	public String authenticationData = "";
	public String formurlEncodedData = "";
	public String formData = "";
	public String linkParams = "[]";
	public String statusParams = "[]";

	// Explicit wait method
	public static WebElement waitForExpectedElement(WebDriver driver, final By locator, int time) {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(time));
		return wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
	}

	public static WebElement waitForExpectedElement(WebDriver driver, WebElement element) {

		initialInputDataClear(driver, element); // if any text in input it will clear
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));
		WebElement d = wait.until(ExpectedConditions.visibilityOf(element));
		String str = element.toString();

		if (str != null && str.toUpperCase().contains("INPUT")) {
			try {
				while (!element.getAttribute("value").equals("") && element.getAttribute("type").equals("text")) {
					element.sendKeys(Keys.BACK_SPACE);
				}
				d.clear();
			} catch (Exception e) {
				System.out.println("Not editable input" + e.getLocalizedMessage());
			}
		}

		return d;

	}

	public static String initialInputDataClear(WebDriver driver, WebElement webElement) {
		String str = webElement.toString();
		try {
			if (str != null && str.toUpperCase().contains("INPUT")) {
				String[] listString = null;
				if (str.contains("xpath")) {
					listString = str.split("xpath:");
				} else if (str.contains("id")) {
					listString = str.split("id:");
				}
				String last = listString[1].trim();
				String xpath = last.substring(0, last.length() - 1);
				if (xpath != null && str.toUpperCase().contains("INPUT")) {
					webElement.clear();
				}
			}
		} catch (Exception e) {
			// System.out.println("Not editable input");
		}
		return str;
	}

	// Explicit wait
	public static WebElement waitForExpectedElement(WebDriver driver, final By locator) {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(120));
		return wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
	}

	// Explicit wait method
	public boolean objectExists(WebDriver driver, final By locator) {
		try {
			waitForPageToLoad();
			WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
			wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	// Explicit wait method (While Script Execution we need to pass time limit)
	public boolean objectExists(WebDriver driver, final By locator, int timeout) {
		try {
			waitForPageToLoad();
			WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(timeout));
			wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	// Explicit wait method (While Script Execution we need to pass time limit
	public void waitForPageToLoad() {
		(new WebDriverWait(getDriver(), Duration.ofSeconds(60))).until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver d) {
				return (((org.openqa.selenium.JavascriptExecutor) getDriver()).executeScript("return document.readyState")
						.equals("complete"));
			}
		});
	}

	/**
	 * Driver instance for Desktop Applications
	 *
	 * @param applicationexePath
	 * @param remoteWiniumDriverPath
	 * @return
	 * @throws Exception
	 */
	public WiniumDriver launchDesktopApp(String applicationexePath, String remoteWiniumDriverPath) throws Exception {
		DesktopOptions options = new DesktopOptions();
		options.setApplicationPath(applicationexePath);
		winiumDriver = new WiniumDriver(new URL(remoteWiniumDriverPath), options);
		return winiumDriver;
	}

	// ============================================= APPIUM SETUP ==============================================//
	/**
	 * Configuring the node and appium path
	 *
	 * @return
	 */
	private String nodeJSPath = System.getenv("NODE_PATH");
	private String appiumServerJSPath = System.getenv("APPIUM_JS_PATH");
	private AppiumDriverLocalService appiumDriverService;


	@SuppressWarnings("unused")
	private AppiumDriverLocalService setUpAppiumDriver(ConfigFilesUtility configFileObj) {
		// Only for iOS/Mac
		if (configFileObj.getProperty("app").contains(".ipa") || isMac()) {
			nodeJSPath = configFileObj.getProperty("nodePath").trim();
			appiumServerJSPath = configFileObj.getProperty("appiumJSPATH").trim();
		}

		AppiumDriverLocalService appiumDriverService = AppiumDriverLocalService
				.buildService(new AppiumServiceBuilder().usingAnyFreePort().usingDriverExecutable(new File(nodeJSPath))
						.withAppiumJS(new File(appiumServerJSPath)));
		return appiumDriverService;
	}

	/**
	 * Used to start Appium Server
	 *
	 * @return boolean appium is started or not
	 */
	/**
	 * used to start appium server before creating a driver for physical device
	 * execution Note - Should install appium 2 server to start Appium
	 */
	public void startAppiumServer() {
		try {
			if (isMac()) {
				nodeJSPath = "/usr/local/bin/node";
				appiumServerJSPath = "/usr/local/lib/node_modules/appium/build/lib/main.js";
			}
			AppiumServiceBuilder builder;
			builder = new AppiumServiceBuilder().withArgument(() -> "--base-path", "/wd/hub");
			// .withArgument(() -> "--use-plugins", "images");
			builder.usingAnyFreePort();
			builder.usingDriverExecutable(new File(nodeJSPath));
			builder.withAppiumJS(new File(appiumServerJSPath));
			HashMap<String, String> environment = new HashMap<String, String>();
			environment.put("PATH", "/usr/local/bin:" + System.getenv("PATH"));
			builder.withEnvironment(environment);

			service = AppiumDriverLocalService.buildService(builder);

			if (service.isRunning()) {
				service.stop();
			}

			service.start();

		} catch (Exception e) {
			// print
		}

	}

	/**
	 * used to stop the Appium server
	 */
	public void stopAppiumServer() {
		if (appiumDriverService != null) {
			appiumDriverService.stop();
			System.out.println(" Stopped Appium Server");
		}
	}

	private void getDriversPath() {
		if (!isWindows()) {
			if (isSolaris() || isUnix()) {
				chromeDriverPath = chromeDriverPath.replace(".exe", "");
				geckoFireFoxDriverPath = geckoFireFoxDriverPath.replace(".exe", "");
			} else if (isMac()) {
				chromeDriverPath = chromeDriverPath.replace("chromedriver.exe", "macChromeDriver");
				geckoFireFoxDriverPath = geckoFireFoxDriverPath.replace("geckodriver.exe", "macGeckodriver");
			}
		}
	}


	public WebDriver launchBrowser(String browserName, ConfigFilesUtility configFileObj) {
		WebDriver driver = getDriver();
		try {
			
			if(DriverInstance.isMobilePlatform()) {
				return driver = DriverInstance.getDriver();
			}

			if (browserName == null) {
				browserName = "chrome";
			}

			getDriversPath();
			GlobalData.primaryInfoData(configFileObj);

			if (browserName.equalsIgnoreCase("chrome")) {

				// System.setProperty("webdriver.chrome.driver", chromeDriverPath);
				//WebDriverManager.chromedriver().setup();

				ChromeOptions options = new ChromeOptions();
				options.addArguments("--remote-allow-origins=*");
				Map<String, Object> prefs = new HashMap<String, Object>();
				prefs.put("download.default_directory", System.getProperty("user.dir") + File.separator
						+ "externalFiles" + File.separator + "downloadFiles");

				options.setExperimentalOption("prefs", prefs);

				if (isWindows()) {
					//// options.addArguments("user-data-dir=C://Users/"+System.getProperty("user.name")+"/AppData/Local/Google/Chrome/User
					//// Data/Default");
				}

				// 1-Allow, 2-Block, 0-default
				options.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
				prefs.put("profile.default_content_setting_values.notifications", 1);
				options.setExperimentalOption("prefs", prefs);

				if (isSolaris() || isUnix()) {

					options.addArguments("start-maximized"); // open Browser in maximized mode
					options.addArguments("disable-infobars"); // disabling infobars
					options.addArguments("--disable-extensions"); // disabling extensions
					options.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
					options.addArguments("--no-sandbox"); // Bypass OS security model
					options.addArguments("--headless"); // this line makes run in linux environment with jenkins
					WebDriverManager.chromedriver().setup();
					driver = new ChromeDriver(options);
				}  else {
					driver  = new ChromeDriver(options);
					Set<Cookie> cookies = driver.manage().getCookies();
					for (Cookie getCookie : cookies)
						driver.manage().addCookie(getCookie);
				}
			

				System.out.println("Chrome Browser is Launched");
			} else if (browserName.equalsIgnoreCase("mozilla")) {
				if (!isWindows()) {
					System.out.println("Edge Browser not supported for this OS.");
					return null;
				}
				WebDriverManager.firefoxdriver().setup();
				driver = new FirefoxDriver();
			} else if (browserName.equalsIgnoreCase("safari")) {
				// Note : Should AllowRemoteAutomation in safari browser DeveloperMenu
				// Directions -- > launchSafariBrowser --> Preferences --> Advanced Tab -->
				// Show Developer Menu --> Click on DevloperMenu --> Enable
				// AllowRemoteAutomation
				// System.setProperty("webdriver.safari.noinstall", "true");
				driver = new SafariDriver();
				// driver.get("http://www.google.com");
				System.out.println("Safari Browser is Launched");
			} else if (browserName.equalsIgnoreCase("ie")) {
				if (!isWindows()) {
					System.out.println("Edge Browser not supported for this OS.");
					return null;
				}
				WebDriverManager.edgedriver().setup();
				EdgeOptions capabilities = new EdgeOptions();
				capabilities.setCapability("ignoreProtectedModeSettings", true);
				capabilities.setCapability("ignoreZoomSetting", true);
				capabilities.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);

				capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);

				driver = new EdgeDriver(capabilities);

				System.out.println("Edge Browser is Launched");
			}
			try {
				driver = SelfHealingDriver.create(driver);
			} catch (Exception e) {
				//OK
			}

			driver.get(configFileObj.getProperty("URL"));

			if (isSolaris() || isUnix()) {

				Dimension d = new Dimension(1382, 744);
				// Resize the current window to the given dimension
				driver.manage().window().setSize(d);
			} else {
				java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
				int screenHeight = screenSize.height;
				int screenWidth = screenSize.width;

				Dimension d = new Dimension(screenWidth, screenHeight);
				// Resize the current window to the given dimension
				driver.manage().window().setSize(d);
				// driver.manage().window().setPosition(new Point(-2000, 0));
				driver.manage().window().maximize();
			}
		} catch (Exception e) {
			reportFailureCreation(browserName, Throwables.getStackTraceAsString(e), "");
		}
		WebSelfHealingDriverInstance.setDriver(driver);
		DriverInstance.setMobilePlatform(false);
		return WebSelfHealingDriverInstance.getDriver();
	}

	private String OS = System.getProperty("os.name").toLowerCase();

	public boolean isWindows() {
		return (OS.indexOf("win") >= 0);
	}

	public boolean isMac() {
		return (OS.indexOf("mac") >= 0);
	}

	public boolean isUnix() {
		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0);
	}

	public boolean isSolaris() {
		return (OS.indexOf("sunos") >= 0);
	}



	// ===================== For Report ========================
	JSONArray jsonArray;

	// For Web
	public void setTestcaseName(String browserName, String testcaseName) {
		

		try {
			if(!DriverInstance.isMobilePlatform()) {
				ExtTest.startTest(testcaseName, browserName);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		
		if(DriverInstance.isMobilePlatform()) {
			jsonArray = ExtTest.getJsonArray();
			return;
		} else {
			GenerateRadomData.setRandomNumber("");
			GenerateRadomData.getRandomNumber();
			jsonArray = ExtTest.getJsonArray();
		}
		
		if (browserName == null) {
			browserName = "chrome";
		}

		String chromeURL = "chrome";
		String mozillaURL = "mozilla";
		String ieURL = "ie";
		String safariURL = "safari";
		String finalURL = "";
		if (browserName.equalsIgnoreCase("chrome")) {
			finalURL = chromeURL;
		} else if (browserName.equalsIgnoreCase("mozilla")) {
			finalURL = mozillaURL;
		} else if (browserName.equalsIgnoreCase("ie")) {
			finalURL = ieURL;
		} else if (browserName.equalsIgnoreCase("safari")) {
			finalURL = safariURL;
		} else {
			finalURL = "winium";
		}

		JSONObject jsonoBj = new JSONObject();
		jsonoBj.put("browser_type", finalURL);
		jsonoBj.put("testcase_name", testcaseName);
		jsonoBj.put("datasets", jsonArray);
		GlobalData.reportData(testcaseName, jsonoBj);
		reportCreation("info", "Browser: " + finalURL);
		if (WebSelfHealingDriverInstance.getDriver() != null) {
			reportCreation("info", "URL: " + getDriver().getCurrentUrl());
		} else {
			reportCreation("fail", "Driver Session not created due to some exception. Please check");
		}
		if (deviceNameReport != null) {
			reportCreation("info", "Device Name : " + deviceNameReport);
			reportCreation("info", "Platform Name : " + platformNameReport);
			reportCreation("info", "Platform Version : " + platformVersionReport);
			reportCreation("info", "Browser Name : " + browserNameReport);
			reportCreation("info", "Execution Mode : " + executionEnvironmentReport);
		}
		// testcaseObj.put(tescaseName, jsonArray);
	}

	public void testLogHeader(String data) {
		reportHeadersCreation("info", data);
	}

	public void printSuccessLogAndReport(String data) {
		reportCreation("pass", data);
		LoggingUtils.logInfo(data);
	}

	public void printSuccessLogAndReport(String elementText, String data) {
		if (data.contains("printMsg")) {
			reportCreation("info", elementText);
		} else {
			reportCreation("pass", data);
		}
		LoggingUtils.logInfo(data);

	}

	public void printValidateLogAndReport(String data) throws Exception {

		if (data.startsWith("Pass:")) {
			data = data.replace("Pass:", "");
			printSuccessLogAndReport(data);
		} else {
			data = data.replace("Fail:", "");
			printFailureLogAndReport(data);
		}
	}

	public void printFailureLogAndReport(String elementText, String data) {

		if (data.contains("printMsg")) {
			reportCreation("info", elementText);
		} else {
			String name = "";
			if (data.toString().length() <= 20) {
				name = data.toString();
			} else {
				name = data.toString().substring(0, 10);
			}
			String base64Data = Utilities.captureScreenshot(getDriver(), name);
			if (base64Data != null) {
				reportFailureCreation("fail", StringEscapeUtils.escapeJava(data), base64Data);
			} else {
				reportCreation("fail", data);
			}
		}

	}

	public void printFailureLogAndReport(String data) {
		WebDriver driver = getDriver();
		if (data == null)
			data = "";
		String name = "";
		if (data.toString().length() <= 20) {
			name = data.toString();
		} else {
			name = data.toString().substring(0, 10);
		}
		String base64Data  = Utilities.captureScreenshot(driver, name);
	
		if (base64Data != null) {
			reportFailureCreation("fail", StringEscapeUtils.escapeJava(data), base64Data);
			LoggingUtils.logBase64(base64Data, StringEscapeUtils.escapeJava(data));
		} else {
			reportCreation("fail", data);
		}

	}

	/***
	 * Fail status for Desktop Application Report
	 *
	 * @param logger
	 * @param data
	 */

	public void printFailureLogAndReportDesktop(String data) {

		String name = "";

		if (data.toString().length() <= 20) {
			name = data.toString();
		} else {
			name = data.toString().substring(0, 10);
		}
		// String base64Data =Utilities.captureScreenshot(driver, name);

		reportFailureCreation("fail", StringEscapeUtils.escapeJava(data),
				Utilities.captureScreenshotDesktopApplication(winiumDriver, name));
		//Assert.assertTrue(false);
	}

	public void printInfoLogAndReport(String data) {

		if (data.startsWith("Pass:")) {
			data = data.replace("Pass:", "");
			printSuccessLogAndReport(data);
		} else if (data.startsWith("Fail:")) {
			data = data.replace("Fail:", "");
			printFailureLogAndReport(data);
		} else {
			reportCreation("info", data);
		}

	}

	public void printScreenReport(String data) {
		ExtTest.createNode(data);
	}

	public void reportHeadersCreation(String result, String data) {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("result_type", "screen");
		jsonObject.put("text", data);
		//System.out.println(data);
		ExtTest.getJsonArray().put(jsonObject);

	}

	public void reportCreation(String result, String data) {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("result_type", result);
		jsonObject.put("text", data);
	//	System.out.println(data);
		 ExtTest.getJsonArray().put(jsonObject);
		  WebDriver driver = getDriver();
		
		if (result.equalsIgnoreCase("pass") && data.trim().startsWith("Validation:")) {
			if (driver != null) {
				String base64Data = Utilities.captureScreenshot(driver, data);
				ExtTest.getTest().log(Status.PASS, "<p style=\"color:green\"><b>" + data + "</br>"
						+ addScreenshot(base64Data, "Validation Screenshot") + "</b></p>");
			}
		} else if (result.equalsIgnoreCase("fail") && data.startsWith(" Validation:")) {
			ExtTest.getTest().log(Status.FAIL, "<p style=\"color:red\"><b>" + data + "</b></p>");
		} else if (result.equalsIgnoreCase("pass")) {
			ExtTest.getTest().log(Status.PASS, "<p style=\"color:purple\"><b>" + data + "</b></p>");
		} else if (result.equalsIgnoreCase("info")) {
			ExtTest.getTest().log(Status.INFO, data);
		} else {
			ExtTest.getTest().log(Status.FAIL, data);
		}

	}

	public void reportFailureCreation(String result, String data, String image) {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("result_type", result);
		jsonObject.put("text", data);
	//	System.out.println(data);
		jsonObject.put("screenshot", image);
		if(ExtTest.getJsonArray() != null)
		 ExtTest.getJsonArray().put(jsonObject);

		ExtTest.getTest().log(Status.FAIL, "<p style=\"color:red\"><b>" + data + "</b></p>");

		ExtTest.getTest().log(Status.FAIL, addScreenshot(image, "Failure Screenshot"));

	}

	// ============= End Report ===============

	public void tearDown() throws Exception {
		getDriver().quit();
	}

	// mouseHover
	public void mouseHover(WebDriver webDriver, WebElement element) {

		Actions action = new Actions(webDriver);
		action.moveToElement(element).build().perform();
	}

	// window
	String parentHandle = "";

	public void windowHandle(WebDriver webDriver) {
		parentHandle = getDriver().getWindowHandle();
		Set<String> handles = getDriver().getWindowHandles();
		for (String windowHandles : handles) {
			System.out.println(windowHandles);
			getDriver().switchTo().window(windowHandles);
		}
	}

	public void switchToParentWindow(WebDriver webDriver) {
		if (parentHandle != null && !parentHandle.isEmpty()) {
			getDriver().switchTo().window(parentHandle);
		}
	}

	// upload a file
	public void uploadFile(String path, String xpath) {
		try {
			WebElement element = waitForExpectedElement(getDriver(), By.xpath(xpath));
			element.sendKeys(path);
		} catch (Exception e) {
			e.getMessage();
		}
	}
	
	private WebDriver getDriver() {
		WebDriver driver = WebSelfHealingDriverInstance.getDriver();
		if(DriverInstance.isMobilePlatform()) {
			driver =  DriverInstance.getDriver();
	    } 
		return driver;
	}

	// Dropdown
	public String dropDownHandle(String xpath, String optionName) {
		Select oSelect = new Select(getDriver().findElement(By.xpath(xpath)));
		oSelect.selectByValue(optionName);
		WebElement option = oSelect.getFirstSelectedOption();
		option.getText();
		return "";
	}

	// handle table validations
	public String tableDataHandle(String xpath) {
		WebDriver driver = getDriver();
		List<WebElement> rows;
		if (xpath.startsWith("/")) {
			waitForExpectedElement(driver, By.xpath(xpath));
			xpath = xpath + "/tbody/tr";
			rows = driver.findElements(By.xpath(xpath));
		} else {
			waitForExpectedElement(driver, By.cssSelector(xpath));
			xpath = xpath + ">tbody>tr";
			rows = driver.findElements(By.cssSelector(xpath));
		}
		// List<WebElement> rows = element.findElements(By.xpath(trXpath));
		// Print data from each row
		reportCreation("info", "Number of rows : " + rows.size());

		for (WebElement row : rows) {
			System.out.print(row + "\t");
			List<WebElement> cols = row.findElements(By.tagName("td"));
			for (WebElement col : cols) {
				System.out.print(col.getText() + "\t");
			}
			System.out.println();
		}
		return "" + rows.size();

	}

	// Switching to iframe by position
	public void switchToIframe(String xpath) {
		WebDriver driver = getDriver();
		try {

			Thread.sleep(25000);
			for (int framePosition = 0; framePosition <= 5; framePosition++) {
				try {
					driver.switchTo().frame(framePosition);
					WebElement ele;
					if (xpath.startsWith("/") || xpath.startsWith("(")) {
						ele = driver.findElement(By.xpath(xpath));
					} else {
						ele = driver.findElement(By.cssSelector(xpath));
					}
					if (ele.isDisplayed()) {
						// driver.switchTo().frame(framePosition);
						System.out.println(framePosition);
						break;
					}
				} catch (Exception e) {
					driver.switchTo().parentFrame();
				}
			}
		} catch (Exception e) {
			driver.switchTo().parentFrame();
		}
	}

	public void alertHandle(boolean isAlertAccept) {
		WebDriver driver = getDriver();
		try {
			Alert alert = driver.switchTo().alert();
			String alertMessage = driver.switchTo().alert().getText();
			System.out.println(alertMessage);
			Thread.sleep(5000);
			if (isAlertAccept) {
				alert.accept();
			} else {
				alert.dismiss();
			}
		} catch (Exception e) {

		}
	}

	public boolean isElementPresent(WebElement e) {
		try {
			boolean isDisplayed = e.isDisplayed();
			isElementDispalyed = isDisplayed;
		} catch (Exception exception) {
			isElementDispalyed = false;
		}
		return isElementDispalyed;
	}

	public boolean skipifElementisNotDisplayed() {
		return isElementDispalyed;
	}

	/**
	 * DataProvider that explicitly sets the browser combinations to be used.
	 *
	 * @param testMethod
	 * @return Two dimensional array of objects with browser, version, and platform
	 *         information
	 * @throws Exception
	 */
	@DataProvider(name = "capabilities", parallel = false)
	public Object[][] sauceBrowserDataProvider(Method testMethod) throws Exception {
		return Utilities.sauceDevicesList();
	}

	String device;
	private String executionEnvironmentReport;
	private String platformNameReport;
	private String deviceNameReport;
	private String platformVersionReport;
	private String browserNameReport;
	private String executionEnvironment;
	//private String bundleId;
	public static String projectPath = System.getProperty("user.dir");

	public WebDriver createDriver(ConfigFilesUtility configFileObj) {
		getDriversPath();
		String capabilitiesFile = projectPath + File.separator + "ConfigFiles" + File.separator
				+ "DeviceCapabilities.json";
		WebDriver driver = getDriver();
		try {
			String data = new String(Files.readAllBytes(Paths.get(capabilitiesFile)));

			if (data == null || data.isEmpty()) {
				String fail = "Please enter a valid JSON in this file: " + capabilitiesFile;
				assertTrue(fail, false);
			}

			JSONObject capabilitiesObj = new JSONObject(data); // outer object

			String driverURL = capabilitiesObj.optString("driverURL");
			if (data.contains("driverURL")) {
				executionEnvironment = "Cloud";
			} else {
				executionEnvironment = "Local";
				if (capabilitiesObj.getString("platformName").equalsIgnoreCase("Android")) {
					capabilitiesObj.put("chromedriverExecutable", chromeDriverPath);
				}
				startAppiumServer();
			}

			driver = createDriverInstance(data, driverURL);
			driver.get(configFileObj.getProperty("URL"));
			return driver;
		} catch (Exception e) {
			printFailureLogAndReport(Throwables.getStackTraceAsString(e));
		}

		return null;

	}

	
	private WebDriver createDriverInstance(String capabilitiesJson, String driverUrl) {
		WebDriver driver = getDriver();
		try {
			String data = capabilitiesJson;

			if (data == null || data.isEmpty()) {
				String fail = "Please enter a valid JSON in this file: " + capabilitiesJson;
				assertTrue(fail, false);
			}

			JSONObject capabilitiesObj = new JSONObject(data); // outer object

			String platformName = capabilitiesObj.getString(MobileCapabilityType.PLATFORM_NAME);
			MutableCapabilities caps = new MutableCapabilities();

			Iterator<String> keys = capabilitiesObj.keys();
			while (keys.hasNext()) {
				String key = keys.next();
				if (key.contains("bundleId")) {
					//bundleId = capabilitiesObj.get(key).toString();
				}
				if (key.contains("driverURL")) {
					continue;
				}
				Object item = capabilitiesObj.get(key);

				if (item instanceof JSONObject) { // Inner object

					JSONObject optionsObj = (JSONObject) item;
					MutableCapabilities options = new MutableCapabilities();
					Iterator<String> optionKeys = optionsObj.keys();
					while (optionKeys.hasNext()) {
						String optionKey = optionKeys.next();
						String optionValue = (String) optionsObj.get(optionKey);
						options.setCapability(optionKey, optionValue);
					}
					caps.setCapability(key, options);
				} else if (item instanceof Integer) {
					int val = (Integer) item;
					caps.setCapability(key, val);

				} else if (item instanceof String) {
					String val = (String) item;
					caps.setCapability(key, val);
				}
			}

			if (executionEnvironment.equalsIgnoreCase("local")) {
				driverUrl = service.getUrl().toString();
			}

			caps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 300);

			if (platformName.equalsIgnoreCase("iOS")) {
				caps.setCapability("wdaEventloopIdleDelay", 3);
				caps.setCapability("waitForQuiescence", false);
				caps.setCapability("shouldUseSingletonTestManager", false);
				caps.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.IOS_XCUI_TEST);
				driver = new IOSDriver(new URL(driverUrl), caps);

				// driver.setSetting("snapshotMaxDepth", 62);
			} else {
				caps.setCapability("appium:automationName", AutomationName.ANDROID_UIAUTOMATOR2);
				driver = new AndroidDriver(new URL(driverUrl), caps);
			}

		} catch (Exception ex) {
			String exception = "Driver not created: " + Throwables.getStackTraceAsString(ex);
			printFailureLogAndReport(exception);
		}
		WebSelfHealingDriverInstance.setDriver(driver);
		
		return driver;

	}


	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {
		WebDriver driver = getDriver();
		try {
			if (driver != null) {
				driver.quit();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public void cleanupObject(Object obj) {
		try {
			String base64Data = Utilities.captureScreenshot(WebSelfHealingDriverInstance.getDriver(), obj.getClass().getSimpleName());
			ExtTest.getTest().log(Status.INFO,
					addScreenshot(base64Data, obj.getClass().getSimpleName() + "- End Test Screenshot"));
			obj = null;
			QFCustomizedCode.screenEndCustomCode(obj);
		} catch (Exception e) {
			// Ok
		}
	}

	public String addScreenshot(String base64Data, String title) {
		String html = "<div class=\"row mb-3\"><div class=\"col-md-3\">\r\n"
				+ "        <a href=\"data:image/png;base64," + base64Data + "\" data-featherlight=\"image\">"
				+ "<span class=\"badge badge-gradient-primary\">" + title + "</span></a>\r\n" + "    </div></div>";
		return html;
	}

	@AfterMethod
	protected void afterMethod(ITestResult result) {
		if (result.getStatus() == ITestResult.FAILURE) {
			//ExtTest.getTest().log(Status.FAIL, result.getThrowable());
		} else if (result.getStatus() == ITestResult.SKIP) {
			//ExtTest.getTest().log(Status.SKIP, "Test skipped " + result.getThrowable());
		} else {
			//ExtTest.getTest().log(Status.PASS, "Test passed");
		}
		com.extentreport.ExtentManager.extentReports.flush();
	}

}
