package com.utilities;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.manybrain.mailinator.client.MailinatorClient;
import com.manybrain.mailinator.client.message.GetInboxRequest;
import com.manybrain.mailinator.client.message.GetMessageRequest;
import com.manybrain.mailinator.client.message.Inbox;
import com.manybrain.mailinator.client.message.Message;
import com.manybrain.mailinator.client.message.Sort;

public class MailnatorAuthentication {
	
	//	String apiKey = "4508812f336a4d76bf4c191931dba4ac";	
	//	int optDigitLegth = 6;
	//	String username = "sravani";

	public String privateMalinator(String apiKey, int otpDigitLength, String username, String title) {
		MailinatorClient mailinatorClient = new MailinatorClient(apiKey);
		Inbox inbox = mailinatorClient.request(new GetInboxRequest("private"));
		return getOTP(mailinatorClient, inbox, otpDigitLength, username, title);
	}

	public String publicMalinator(String apiKey, int otpDigitLength, String username, String title) {
		MailinatorClient mailinatorClient = new MailinatorClient(apiKey);
		Inbox inbox = mailinatorClient.request(GetInboxRequest.builder().domain("public").inbox(username).limit(1).skip(0).sort(Sort.DESC).build());
		return getOTP(mailinatorClient, inbox, otpDigitLength, username, title);
	}
	
	
	private String getOTP(MailinatorClient mailinatorClient, Inbox inbox, int otpDigitLength, String username, String title) {
		String otp = "";
		List<Message> filteredMessages = inbox.getMsgs().stream().filter(message -> (message.getTo().equalsIgnoreCase(username) && message.getSubject().startsWith(title))).collect(Collectors.toList());
		if (filteredMessages.size() > 0) {
				Message latestMessage = filteredMessages.get(filteredMessages.size() -1);
				Message m2 = mailinatorClient.request(new GetMessageRequest(inbox.getDomain(), latestMessage.getTo(), latestMessage.getId()));
				String parts = m2.getParts().get(0).getBody();
				Pattern pattern = Pattern.compile("\\d{" + otpDigitLength + "}");
				Matcher matcher = pattern.matcher(parts);
				if (matcher.find()) {
					otp = matcher.group();
					System.out.println("OTP: " + otp);
				} else {
					System.out.println("OTP not found");
				}
		} else {
			System.out.println("No messages found to this user: " + username);
		}
		
		return otp;
	}
}
