package com.utilities;


import java.io.File;
import java.util.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggingUtils {

	//private static final Logger LOGGER = LoggerFactory.getLogger("step");

	private LoggingUtils() {
		//statics only
	}

	public static void log(File file, String message) {
		//LOGGER.info("RP_MESSAGE#FILE#{}#{}", file.getAbsolutePath(), message);
	}

	public static void log(byte[] bytes, String message) {
		//LOGGER.info("RP_MESSAGE#BASE64#{}#{}", Base64.getEncoder().encodeToString(bytes), message);
	}

	public static void logBase64(String base64, String message) {
		//LOGGER.error("RP_MESSAGE#BASE64#{}#{}", base64, message);
		//Assert.fail(message);
	}
	
	public static void logInfo(String message) {
		//LOGGER.info(message);
	}
	
	
	public static void logFail(String message) {
		//LOGGER.error(message);
		//Assert.fail(message);
	}
	
	
	
}