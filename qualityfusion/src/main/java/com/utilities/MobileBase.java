package com.utilities;

import static org.testng.AssertJUnit.assertTrue;

import java.io.File;
import java.lang.reflect.Method;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.ScreenOrientation;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;

import com.aventstack.extentreports.Status;
import com.configurations.GlobalData;
import com.extentreport.ExtTest;
import com.google.common.base.Throwables;

import groovy.json.StringEscapeUtils;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.AutomationName;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;

public class MobileBase {

	public static String projectPath = System.getProperty("user.dir");

	public static final int WAIT_5_SECS = 5;
	public static final int WAIT_10_SECS = 10;
	public static final int WAIT_15_SECS = 15;
	public static final int WAIT_20_SECS = 20;
	public static final int WAIT_25_SECS = 25;

	private static String appiumServerJSPath = System.getenv("APPIUM_JS_PATH");
	private static String nodeJSPath = System.getenv("NODE_PATH");
	private static AppiumDriverLocalService service;
	private static String executionEnvironment;
	private static AppiumDriver driver;
	@SuppressWarnings("unused")
	private static String bundleId;

	/**
	 * used to start appium server before creating a driver for physical device
	 * execution Note - Should install appium 2 server to start Appium
	 */
	public static void startAppiumServer() {
		try {
			if (isMac()) {
				nodeJSPath = "/usr/local/bin/node";
				appiumServerJSPath = "/usr/local/lib/node_modules/appium/build/lib/main.js";
			}
			AppiumServiceBuilder builder;
			builder = new AppiumServiceBuilder().withArgument(() -> "--base-path", "/wd/hub");
			// .withArgument(() -> "--use-plugins", "images");
			builder.usingAnyFreePort();
			builder.usingDriverExecutable(new File(nodeJSPath));
			builder.withAppiumJS(new File(appiumServerJSPath));
			HashMap<String, String> environment = new HashMap<String, String>();
			environment.put("PATH", "/usr/local/bin:" + System.getenv("PATH"));
			builder.withEnvironment(environment);

			service = AppiumDriverLocalService.buildService(builder);

			if (service.isRunning()) {
				service.stop();
			}

			service.start();

		} catch (Exception e) {
			// print
		}

	}

	/**
	 * 
	 * @param executionEnvironment - by using can identify the environment of
	 *                             execution where it is like Local,
	 *                             SauceLabs,Kobiton...
	 * @param driverConfFileName   - capabilities file
	 * @param driverUrl            - URL to connect device
	 * @return
	 */
	public AppiumDriver createDriver() {

		
		String platform = Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest().getParameter("browser");
		if(platform == null) {
			platform = "Android";
		}
		String capabilitiesFile = projectPath + File.separator + "ConfigFiles" + File.separator + platform + "DeviceCapabilities.json";

		try {
			String data = new String(Files.readAllBytes(Paths.get(capabilitiesFile)));

			if (data == null || data.isEmpty()) {
				String fail = "Please enter a valid JSON in this file: " + capabilitiesFile;
				assertTrue(fail, false);
			}

			JSONObject capabilitiesObj = new JSONObject(data); // outer object
			String driverURL = capabilitiesObj.optString("driverURL");
			if (data.contains("driverURL")) {
				executionEnvironment = "Cloud";
				printInfoLog("Cloud server Execution");
			} else {
				executionEnvironment = "Local";
				printInfoLog("Local Execution");
			}
			if (executionEnvironment.equalsIgnoreCase("local")) {
				startAppiumServer();
			}

			return driver = createDriverInstance(data, driverURL);

		} catch (Exception e) {
			printInfoLog(Throwables.getStackTraceAsString(e));
		}
		return null;
	}

	/**
	 * This method will create a Android/iOS driver and launch provided application
	 * in registered device
	 * 
	 * Should install Appium 2 server and drivers by using below commands -- appium
	 * driver install xcuitest - appium driver install uiautomator2
	 * 
	 * @param capabilitiesFile - all the desired capabilities in a file format with
	 *                         JSON syntax
	 * @param driverUrl        - used to cloud device connection
	 * @return AppiumDriver - Creates a new instance based on Appium server URL and
	 *         {@code capabilities}
	 */
	private AppiumDriver createDriverInstance(String capabilitiesJson, String driverUrl) {

		try {
			
			String nodeUrl = "";

			if (DriverInstance.getDriver() == null) {
				String data = capabilitiesJson;

				if (data == null || data.isEmpty()) {
					String fail = "Please enter a valid JSON in this file: " + capabilitiesJson;
					assertTrue(fail, false);
				}

				JSONObject capabilitiesObj = new JSONObject(data); // outer object

				String platformName = capabilitiesObj.getString(MobileCapabilityType.PLATFORM_NAME);
				MutableCapabilities caps = new MutableCapabilities();

				Iterator<String> keys = capabilitiesObj.keys();
				while (keys.hasNext()) {
					String key = keys.next();
					if (key.contains("bundleId")) {
						bundleId = capabilitiesObj.get(key).toString();
					}
					
					if (key.contains("nodeURL")) {
						nodeUrl = capabilitiesObj.get(key).toString();
					}
					if (key.contains("driverURL")) {
						continue;
					}
					Object item = capabilitiesObj.get(key);

					if (item instanceof JSONObject) { // Inner object

						JSONObject optionsObj = (JSONObject) item;
						MutableCapabilities options = new MutableCapabilities();
						Iterator<String> optionKeys = optionsObj.keys();
						printInfoLog("Capabilities:");
						while (optionKeys.hasNext()) {
							String optionKey = optionKeys.next();
							String optionValue = (String) optionsObj.get(optionKey);
							printInfoLog(optionKey + ": " + optionValue);
							options.setCapability(optionKey, optionValue);
						}
						caps.setCapability(key, options);
					} else if (item instanceof Integer) {
						int val = (Integer) item;
						caps.setCapability(key, val);
						printInfoLog(key + ": " + val);

					} else if (item instanceof String) {
						String val = (String) item;
						caps.setCapability(key, val);
						printInfoLog(key + ": " + val);
					}
				}

				if (executionEnvironment.equalsIgnoreCase("local")) {
					driverUrl = service.getUrl().toString();
				}

				printInfoLog("Mobile Type: " + platformName);
				if (platformName.equalsIgnoreCase("iOS")) {
					caps.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.IOS_XCUI_TEST);
					driver = new IOSDriver(new URL(driverUrl), caps);

					// driver.setSetting("snapshotMaxDepth", 62);
				} else {
					caps.setCapability("appium:automationName", AutomationName.ANDROID_UIAUTOMATOR2);
					if(nodeUrl.isEmpty()) {
						driver = new AndroidDriver(new URL(driverUrl), caps);
					} else { // Self Heal
						driver = new AndroidDriver(new URL(nodeUrl), caps);
					}
				}
		

			} else {
				return DriverInstance.getDriver();
			}
		} catch (Exception ex) {
			String exception = "Driver not created: " + Throwables.getStackTraceAsString(ex);
			printFailureLogAndReport(exception);
		}
		if(driver != null) {
			DriverInstance.setDriver(driver);
		}
		DriverInstance.setMobilePlatform(true);
		return driver;

	}

	/**
	 * Used to reset app for Android and iOS For iOS need to pass parameter bundleId
	 * For Android need to pass parameter package name
	 */
	public void resetApp(AppiumDriver driver) {
		if(driver!= null) {
			driver.quit();
		}
		DriverInstance.setDriver(null);
	}

	@SuppressWarnings("deprecation")
	public void resetApp1(AppiumDriver driver) {

	        if (driver != null && driver instanceof AndroidDriver) {
	            ((AndroidDriver) driver).resetApp();
	            ((AndroidDriver) driver).rotate(ScreenOrientation.PORTRAIT);
	        } else {
	            driver = ((IOSDriver) driver);
	            
	            String bundleId = (String) driver.getCapabilities().getCapability("appium:bundleId");
	            ((IOSDriver) driver).terminateApp(bundleId);
	            ((IOSDriver) driver).activateApp(bundleId);
	        }
	    }
	
	
	/**
	 * Close current execution driver connection
	 */
	public void closeDriver() {
		try {
			DriverInstance.getDriver().quit();
		} catch (Exception e) {
			// OK
		}
		DriverInstance.setDriver(null);
	}

	/**
	 * used to close appium server session if exists and close driver
	 */

	@AfterSuite
	public void closeDriverAndSession() {

		AppiumDriver driver = DriverInstance.getDriver();

		if (driver != null) {
			closeDriver();
		}

		if (service != null && service.isRunning()) {
			service.stop();
		}

	}

	@AfterMethod
	public void afterMethod(Method method, ITestResult result) throws Exception {

		if (result.getStatus() == ITestResult.FAILURE) {
			DriverInstance.setJobStatus(true);
		}
	}

	public static boolean isWindows() {
		return (OS.indexOf("win") >= 0);
	}

	public static boolean isUnix() {
		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0);
	}

	public static boolean isSolaris() {
		return (OS.indexOf("sunos") >= 0);
	}

	// For New Reports

	/**************************************
	 * Reports
	 ******************************************/
	// For Mobile
	public void setTestcaseName(String tescaseName, ConfigFilesUtility confObj) {
		GenerateRadomData.setRandomNumber("");
		GenerateRadomData.getRandomNumber();
		JSONObject jsonoBj = new JSONObject();
		ExtTest.startTest(tescaseName, "Mobile");
		JSONArray jsonArray = ExtTest.getJsonArray();
		jsonoBj.put("testcase_name", tescaseName);
		jsonoBj.put("datasets", jsonArray);
		GlobalData.reportData(tescaseName, jsonoBj);
		GlobalData.primaryInfoData(confObj);
		jsonoBj.put("browser_type", "Mobile");
		

		// reportCreation("DeviceName", device);
	}

	// These methods used to write the extent reports and logger
	public void testPass(String message) {
		reportCreation("pass", message);

	}

	public void testInfo(String message) {
		reportCreation("info", message);

	}

	public void testFail(String message, AppiumDriver mobileDriver) {

		Utilities.setMobilePlatform();
		String base64Data = Utilities.captureScreenshot(mobileDriver, message + " is Failed");

		reportFailureCreation("fail", message, base64Data);

		// reportFailureCreation("fail",
		// message,Utilities.captureScreenshot(mobileDriver, message + " is Failed"));
	}

	// private static ExtentTest extentTest;
	// ScreenName Header for Reports
	public void testLogHeader(String data) {

		reportHeadersCreation("info", data);
		// test.log(LogStatus.INFO, "<b style = 'background-color: #ffffff; color :
		// #ff8f00 ; font-size : 18px' >"+ data + "</b>");
	}

	public void reportHeadersCreation(String result, String data) {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("result_type", "screen");
		jsonObject.put("text", data);
		// LoggingUtils.logInfo(result + ": " + data);
		 ExtTest.getJsonArray().put(jsonObject);

	}

	/*
	 * public void reportCreation(String result, String data) { JSONObject
	 * jsonObject = new JSONObject(); jsonObject.put("result_type", result);
	 * jsonObject.put("text", data); jsonArray.put(jsonObject); }
	 */

	public static void reportCreation(String result, String data) {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("result_type", result);
		jsonObject.put("text", data);
		LoggingUtils.logInfo(result + ": " + data);
		ExtTest.getJsonArray().put(jsonObject);
	}

	public static void reportFailureCreation(String result, String data, String image) {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("result_type", result);
		jsonObject.put("text", data);
		jsonObject.put("screenshot", image);
		LoggingUtils.logFail(result + ": " + data);
		LoggingUtils.logFail(result + ": " + image);
		System.out.println("Failed Testcase Execution and logged screenshot: " + data);
		 ExtTest.getJsonArray().put(jsonObject);
	}

	/***********************************
	 * Find the Client OS
	 ****************************************/

	private static final String OS = System.getProperty("os.name").toLowerCase();

	private static boolean isMac() {
		return (OS.indexOf("mac") >= 0);
	}

	
	public void uploadImageFromGallery() {
		// List<MobileElement> pic=
		// appiumDriver.findElements(By.className("//android.view.View"));
		// pic.get(1).click(); // click based on index of image
		driver.findElement(By.xpath("//android.widget.LinearLayout[1]/android.view.view[1][@clickable='True']"))
				.click();
	}

	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {
		System.out.println("=============Tear+===================");
	}

	public void elementWait() {
		try {
			Thread.sleep(5000);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void print(String text) {
		System.out.println(text);
	}

	public void sleep(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			// OK
		}
	}

	
	// window
	String parentHandle = "";

	public void windowHandle(AppiumDriver driver) {
		parentHandle = driver.getWindowHandle();
		Set<String> handles = driver.getWindowHandles();
		for (String windowHandles : handles) {
			System.out.println(windowHandles);
			driver.switchTo().window(windowHandles);
		}
	}

	public void switchToParentWindow(AppiumDriver driver) {
		if (parentHandle != null && !parentHandle.isEmpty()) {
			driver.switchTo().window(parentHandle);
		}
	}

	private boolean isElementDispalyed = false;

	public boolean skipifElementisNotDisplayed() {
		return isElementDispalyed;
	}

	public static void printSuccessLogAndReport(String data) {

		reportCreation("pass", data);
		ExtTest.getTest().log(Status.PASS, "<p style=\"color:purple\"><b>" + data + "</b></p>");
	}

	public static void printInfoLog(String data) {

		reportCreation("info", data);
		ExtTest.getTest().log(Status.INFO, "<p style=\"color:black\"><b>" + data + "</b></p>");
	}

	public static void printSuccessLogAndReport(String elementText, String data) {
		if (data.contains("printMsg")) {
			reportCreation("info", elementText);
		} else {
			reportCreation("pass", data);
		}

	}

	public static void printValidateLogAndReport(String data) throws Exception {

		if (data.startsWith("Pass:")) {
			data = data.replace("Pass:", "");
			printSuccessLogAndReport(data);
		} else {
			data = data.replace("Fail:", "");
			printFailureLogAndReport(data);
		}
	}

	public static void printFailureLogAndReport(String elementText, String data) {

		if (data.contains("printMsg")) {
			reportCreation("info", elementText);
		} else {
			String name = "";
			if (data.toString().length() <= 20) {
				name = data.toString();
			} else {
				name = data.toString().substring(0, 10);
			}
			Utilities.setMobilePlatform();
			String base64Data = Utilities.captureScreenshot(DriverInstance.getDriver(), name);

			reportFailureCreation("fail", StringEscapeUtils.escapeJava(data), base64Data);
		}

	}

	public static void printFailureLogAndReport(String data) {
		if (data == null)
			data = "";

		String name = "";
		if (data.toString().length() <= 20) {
			name = data.toString();
		} else {
			name = data.toString().substring(0, 10);
		}

		Utilities.setMobilePlatform();
		String base64Data = Utilities.captureScreenshot(DriverInstance.getDriver(), name);
		ExtTest.getTest().log(Status.FAIL, "<p style=\"color:red\"><b>" + data + "</br>"
				+ addScreenshot(base64Data, "Failure Screenshot") + "</b></p>");

		MobileDriverInstance.setJobStatus(true);

		reportFailureCreation("fail", StringEscapeUtils.escapeJava(data), base64Data);
	}

	public void printInfoLogAndReport(String data) {
		if (data.startsWith("Pass:")) {
			data = data.replace("Pass:", "");
			printSuccessLogAndReport(data);
		} else if (data.startsWith("Fail:")) {
			data = data.replace("Fail:", "");
			printFailureLogAndReport(data);
		} else {
			reportCreation("info", data);
		}

	}

	public void cleanupObject(Object obj) {
		obj = null;
	}

	@AfterSuite
	public void afterSuiteRuns() {
		try {
			MobileDriverInstance.getDriver().close();
			MobileDriverInstance.getDriver().quit();

		} catch (Exception e) {
			// Ok
		}

	}

	public static String addScreenshot(String base64Data, String title) {
		String html = "<div class=\"row mb-3\"><div class=\"col-md-3\">\r\n"
				+ "        <a href=\"data:image/png;base64," + base64Data + "\" data-featherlight=\"image\">"
				+ "<span class=\"badge badge-gradient-primary\">" + title + "</span></a>\r\n" + "    </div></div>";
		return html;
	}

	@AfterMethod
	protected void afterMethod(ITestResult result) {
		if (result.getStatus() == ITestResult.FAILURE) {
			ExtTest.getTest().log(Status.FAIL, result.getThrowable());
		} else if (result.getStatus() == ITestResult.SKIP) {
			ExtTest.getTest().log(Status.SKIP, "Test skipped " + result.getThrowable());
		} else {
			// ExtTest.getTest().log(Status.PASS, "Test passed");
		}
		com.extentreport.ExtentManager.extentReports.flush();
	}

}
