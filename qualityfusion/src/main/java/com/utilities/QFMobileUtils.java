package com.utilities;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.Pause;
import org.openqa.selenium.interactions.PointerInput;
import org.openqa.selenium.interactions.PointerInput.Kind;
import org.openqa.selenium.interactions.PointerInput.MouseButton;
import org.openqa.selenium.interactions.PointerInput.Origin;
import org.openqa.selenium.interactions.Sequence;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;

import com.google.common.collect.ImmutableList;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.HideKeyboardStrategy;
import io.appium.java_client.remote.SupportsContextSwitching;

public class QFMobileUtils {

	private String bundleId;

	// Wait Methods

	/**
	 * used to Implementations should wait until the condition evaluates to a value
	 * If the condition does not become true within a certain time
	 * 
	 * @param expectedCondition - condition to satisfy
	 * @param timeout           - Sets how long to wait for the evaluated condition
	 */
	public void fluentWait(AppiumDriver driver, final ExpectedCondition<?> expectedCondition, int timeout) {
		Wait<WebDriver> wait = new FluentWait<WebDriver>(DriverInstance.getDriver())
				.withTimeout(Duration.ofSeconds(timeout)).pollingEvery(Duration.ofMillis(200))
				.ignoring(NoSuchElementException.class);
		wait.until(expectedCondition);
	}

	/**
	 * used to Implementations should wait until the condition evaluates to a value
	 * If the condition does not become true within a certain time
	 * 
	 * @param expectedCondition - condition to satisfy
	 * @param timeout           - Sets how long to wait for the evaluated condition
	 */
	public void fluentWaitIgnoreStale(final ExpectedCondition<?> expectedCondition, int timeout) {
		Wait<WebDriver> wait = new FluentWait<WebDriver>(DriverInstance.getDriver())
				.withTimeout(Duration.ofSeconds(timeout)).pollingEvery(Duration.ofMillis(200))
				.ignoring(NoSuchElementException.class).ignoring(StaleElementReferenceException.class);
		wait.until(expectedCondition);
	}

	/**
	 * 
	 * @param expectedCondition - condition to satisfy
	 * @param timeOut           - Sets how long to wait for the evaluated condition
	 */
	public void waitForElement(ExpectedCondition<?> expectedCondition, int timeOut) {
		try {
			WebDriverWait wait = new WebDriverWait(DriverInstance.getDriver(), Duration.ofSeconds(timeOut));
			wait.until(expectedCondition);
		} catch (Exception e) {
			System.out.println("" + e.getLocalizedMessage());
			// OK
		}
	}

	/**
	 * used to wait specified time
	 * 
	 * @param secs - takes the integer value to wait
	 */
	public void hardWait(long secs) {
		try {
			Thread.sleep(secs);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * <p>
	 * This method used to clear the data existing text in input and enter provided
	 * data after clearing the text in input
	 * 
	 * @param element    - used to identify the Mobile Element
	 * @param input      - to enter the provided text
	 * @param reportDesc - to print the report
	 */
	public static void enterInput(AppiumDriver appiumDriver, WebElement element, String input) {
		try {
			tapActionElement(appiumDriver, element);
		} catch (Exception e) {
			// OK
		}
		element.clear();
		element.sendKeys(input);
		hideKeyboard(appiumDriver);
	}

	private static void hideKeyboard(AppiumDriver appiumDriver) {
		try {
			if (appiumDriver instanceof AndroidDriver) {
				((AndroidDriver) appiumDriver).hideKeyboard();
			} else {
				//((IOSDriver) appiumDriver).findElementByAccessibilityId("Hide keyboard").click();
				((IOSDriver) appiumDriver).hideKeyboard(HideKeyboardStrategy.TAP_OUTSIDE);
				//((IOSDriver) appiumDriver).hideKeyboard(HideKeyboardStrategy.PRESS_KEY, "Done");
			}
		} catch (Exception e) {
			// OK
		}
	}

	public void sendKeys(AppiumDriver appiumDriver, String xpath, String inputValue) {
		fluentWait(appiumDriver, ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)), 20);
		enterInput(appiumDriver, appiumDriver.findElement(By.xpath(xpath)), inputValue);
	}

	public String validateAndClickAction(AppiumDriver appiumDriver, String xpath, String data,
			boolean isClick) {
		fluentWait(appiumDriver, ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(xpath)), 10);
		WebElement element = appiumDriver.findElement(By.xpath(xpath));
		String text = element.getText();

		if (text.equals(data)) {
			if (isClick) {
				element.click();
			}

			return "Pass: " + data + " is validated as expected";

		} else {
			return "Fail: Expected " + data + " Actual: " + text;
		}
	}

	public String isElementDisplayed(AppiumDriver appiumDriver, String xpath, String fieldName,
			boolean isClick) {
		fluentWait(appiumDriver, ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(xpath)), 20);
		WebElement element = appiumDriver.findElement(By.xpath(xpath));
		String text = "";
		if (element.isDisplayed()) {
			text = "Pass: " + fieldName + " is displayed";
			if (isClick) {
				element.click();
			}
		} else {
			text = "Fail: " + fieldName + " is not displayed";
		}
		return text;
	}

	public String isElementNotDisplayed(AppiumDriver appiumDriver, String xpath, String fieldName) {
		WebElement element = appiumDriver.findElement(By.xpath(xpath));
		String text = "";
		if (element.isDisplayed()) {
			text = "Fail: " + fieldName + " is displayed";
		} else {
			fluentWait(appiumDriver, ExpectedConditions.invisibilityOf(appiumDriver.findElement(By.xpath(xpath))), 10);
			text = "Pass: " + fieldName + " is not displayed";
		}
		return text;
	}

	/**
	 * used to click like button
	 * 
	 * @param element
	 * @param reportDesc
	 */

	private void scrollDirection(AppiumDriver appiumDriver, String xpath, ScrollDirection direction) {
		print("Scroll: " + direction);

		try {
			// Hide Keyboard before scrolling UI
			try {
				((AndroidDriver) appiumDriver).hideKeyboard();
			} catch (Exception e) {
				try {
					hideKeyboard();
				} catch (Exception e1) {
					// TODO: handle exception
				}
			}

			int maxSwipes = 5;
			waitForElement(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)), 2);
			for (int i = 0; i < maxSwipes; i++) {
				scroll(direction, 0.3d);
				if (validateElementIsDisplayed(appiumDriver, appiumDriver.findElement(By.xpath(xpath)))) {
					isElementDispalyed = true;
					break;
				} else {
					scroll(direction, 0.3d);
				}
			}

		} catch (Exception e1) {
			System.err.println("Element not found");
		}
	}



	public String validateInput(AppiumDriver appiumDriver, String xpath, String data) {
		fluentWait(appiumDriver, ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)), 20);
		WebElement element = appiumDriver.findElement(By.xpath(xpath));
		String text = element.getText();

		if(text.equals(data)) {
			return "Pass: " + data + " is validated as expected";
		} else {
			return "Fail: Expected " + data + " Actual: " + text; 
		}
	}

	
	public void scrollDown(AppiumDriver driver, String xpath) {
		scrollDirection(driver, xpath, ScrollDirection.DOWN);
	}

	public void scrollUp(AppiumDriver driver, String xpath) {
		scrollDirection(driver, xpath, ScrollDirection.UP);
	}

	public void clickAction(AppiumDriver driver, String xpath) {
		try {
			driver.findElement(By.xpath(xpath)).click();
		} catch (Exception e) {
			tapActionElement(driver, driver.findElement(By.xpath(xpath)));
		}
	}

	public void clickAction(AppiumDriver driver, String xpath, boolean isClick) {
		if (isClick) {
			try {
				driver.findElement(By.xpath(xpath)).click();
			} catch (Exception e) {
				e.printStackTrace();
				tapActionElement(driver, driver.findElement(By.xpath(xpath)));
			}
		} 
			
	}

	public static void tapActionElement(AppiumDriver driver, WebElement el) {
		int centerX = el.getRect().x + (el.getSize().width / 2);
		int centerY = el.getRect().y + (el.getSize().height / 2);
		tapAction(driver, centerX, centerY);
	}

	// GENERAL METHODS

	// SCROLL methods

	private static Duration SCROLL_DUR = Duration.ofMillis(1000);
	private static double SCROLL_RATIO = 0.8;
	private static int ANDROID_SCROLL_DIVISOR = 3;
	private static Dimension windowSize;

	private static Dimension getWindowSize() {
		if (windowSize == null) {
			windowSize = DriverInstance.getDriver().manage().window().getSize();
		}
		return windowSize;
	}

	protected static void swipe(Point start, Point end, Duration duration) {

		boolean isAndroid = DriverInstance.getDriver() instanceof AndroidDriver;

		PointerInput input = new PointerInput(Kind.TOUCH, "finger1");
		Sequence swipe = new Sequence(input, 0);
		swipe.addAction(input.createPointerMove(Duration.ZERO, Origin.viewport(), start.x, start.y));
		swipe.addAction(input.createPointerDown(MouseButton.LEFT.asArg()));
		if (isAndroid) {
			duration = duration.dividedBy(ANDROID_SCROLL_DIVISOR);
		} else {
			swipe.addAction(new Pause(input, duration));
			duration = Duration.ZERO;
		}
		swipe.addAction(input.createPointerMove(duration, Origin.viewport(), end.x, end.y));
		swipe.addAction(input.createPointerUp(MouseButton.LEFT.asArg()));
		DriverInstance.getDriver().perform(ImmutableList.of(swipe));
	}

	public static String getScreenShot() {
		try {
			String base64Screenshot = DriverInstance.getDriver().getScreenshotAs(OutputType.BASE64);
			return base64Screenshot;
		} catch (Exception e) {
			// OK
		}
		return null;
	}

	protected static void swipe(double startXPct, double startYPct, double endXPct, double endYPct, Duration duration) {
		Dimension size = getWindowSize();
		Point start = new Point((int) (size.width * startXPct), (int) (size.height * startYPct));
		Point end = new Point((int) (size.width * endXPct), (int) (size.height * endYPct));
		swipe(start, end, duration);
	}

	public void enter(AppiumDriver appiumDriver, String xpath) {
		Actions action = new Actions(appiumDriver);
		action.sendKeys(Keys.ENTER).perform();
	}

	protected static void scroll(ScrollDirection dir, double distance) {
		if (distance < 0 || distance > 1) {
			throw new Error("Scroll distance must be between 0 and 1");
		}
		Dimension size = getWindowSize();
		Point midPoint = new Point((int) (size.width * 0.5), (int) (size.height * 0.5));
		int top = midPoint.y - (int) ((size.height * distance) * 0.5);
		int bottom = midPoint.y + (int) ((size.height * distance) * 0.5);
		int left = midPoint.x - (int) ((size.width * distance) * 0.5);
		int right = midPoint.x + (int) ((size.width * distance) * 0.5);
		if (dir == ScrollDirection.UP) {
			swipe(new Point(midPoint.x, top), new Point(midPoint.x, bottom), SCROLL_DUR);
		} else if (dir == ScrollDirection.DOWN) {
			swipe(new Point(midPoint.x, bottom), new Point(midPoint.x, top), SCROLL_DUR);
		} else if (dir == ScrollDirection.LEFT) {
			swipe(new Point(left, midPoint.y), new Point(right, midPoint.y), SCROLL_DUR);
		} else {
			swipe(new Point(right, midPoint.y), new Point(left, midPoint.y), SCROLL_DUR);
		}
	}

	protected static void scroll(ScrollDirection dir) {
		scroll(dir, SCROLL_RATIO);
	}

	protected static void scroll() {
		scroll(ScrollDirection.DOWN, SCROLL_RATIO);
	}

	public enum ScrollDirection {
		UP, DOWN, LEFT, RIGHT
	}

	public static void tapAction(AppiumDriver driver, int pointA_X, int pointA_Y) {
		PointerInput finger = new PointerInput(org.openqa.selenium.interactions.PointerInput.Kind.TOUCH, "finger");
		org.openqa.selenium.interactions.Sequence clickPosition = new org.openqa.selenium.interactions.Sequence(finger,
				1);
		clickPosition.addAction(finger.createPointerMove(Duration.ZERO, Origin.viewport(), pointA_X, pointA_Y))
				.addAction(finger.createPointerDown(MouseButton.LEFT.asArg()))
				.addAction(finger.createPointerUp(MouseButton.LEFT.asArg()));
		driver.perform(Arrays.asList(clickPosition));
	}

	// vertical scroll to specific element
	public void scrollVerical(AppiumDriver driver, WebElement element) {

		int centerX = element.getRect().x + (element.getSize().width / 2);
		double startY = element.getRect().y + (element.getSize().height * 0.9);
		double endY = element.getRect().y + (element.getSize().height * 0.1);

		PointerInput finger = new PointerInput(PointerInput.Kind.TOUCH, "finger");
		Sequence swipe = new Sequence(finger, 1);
		swipe.addAction(
				finger.createPointerMove(Duration.ofSeconds(0), PointerInput.Origin.viewport(), centerX, (int) startY));

		swipe.addAction(finger.createPointerDown(0));
		swipe.addAction(
				finger.createPointerMove(Duration.ofMillis(700), PointerInput.Origin.viewport(), centerX, (int) endY));
		driver.perform(Arrays.asList(swipe));
	}

	// horizontal scroll to specific element
	public void scrollHorizontal(AppiumDriver driver, WebElement element) {

		int centerY = element.getRect().y + (element.getSize().height / 2);
		double startX = element.getRect().x + (element.getSize().width * 0.9);
		double endX = element.getRect().x + (element.getSize().width * 0.1);

		PointerInput finger = new PointerInput(PointerInput.Kind.TOUCH, "finger");
		Sequence swipe = new Sequence(finger, 1);
		swipe.addAction(
				finger.createPointerMove(Duration.ofSeconds(0), PointerInput.Origin.viewport(), (int) startX, centerY));

		swipe.addAction(finger.createPointerDown(0));
		swipe.addAction(
				finger.createPointerMove(Duration.ofMillis(700), PointerInput.Origin.viewport(), (int) endX, centerY));
		driver.perform(Arrays.asList(swipe));
	}

	public int getRandomNumber(int min, int max) {
		Random rand = new Random();
		int randomNumber = rand.nextInt((max - min) + 1) + min;
		return randomNumber - 1;
	}

	public String changeDateFormat(String dateTxt, String currentFormat, String requiredFormat) {
		try {
			Date date = new SimpleDateFormat(currentFormat).parse(dateTxt);
			String expectedDate = new SimpleDateFormat(requiredFormat).format(date);
			return expectedDate;
		} catch (Exception e) {
			return dateTxt;
		}
	}

	public void hideKeyboard() {
		try {
			if (DriverInstance.getDriver() instanceof AndroidDriver) {
				((AndroidDriver) DriverInstance.getDriver()).hideKeyboard();
			} else {
				scroll(ScrollDirection.LEFT, 0.5d);
			}
		} catch (Exception e) {
			// OK
		}
	}

	@AfterMethod
	public void afterMethod(Method method, ITestResult result) throws Exception {

		if (result.getStatus() == ITestResult.FAILURE) {
			DriverInstance.setJobStatus(true);
		}
	}

	public static boolean isWindows() {
		return (OS.indexOf("win") >= 0);
	}

	public static boolean isUnix() {
		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0);
	}

	public static boolean isSolaris() {
		return (OS.indexOf("sunos") >= 0);
	}

	/***********************************
	 * Find the Client OS
	 ****************************************/

	private static final String OS = System.getProperty("os.name").toLowerCase();


	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {
		System.out.println("=============Tear+===================");
	}

	public void elementWait() {
		try {
			Thread.sleep(5000);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void print(String text) {
		System.out.println(text);
	}

	public void sleep(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			// OK
		}
	}

	

	@SuppressWarnings("deprecation")
	public void implicitWait(AppiumDriver driver, int timeOut) {
		driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
	}

	public boolean validateElementIsDisplayed(AppiumDriver driver, WebElement element) {
		try {
			fluentWait(driver, ExpectedConditions.visibilityOf(element), 10);
			if (element.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			// OK
		}
		return false;

	}

	

	public String randomText(int length) {
		char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
		StringBuilder sb = new StringBuilder(length);
		Random random = new Random();
		for (int i = 0; i < length; i++) {
			char c = chars[random.nextInt(chars.length)];
			sb.append(c);
		}
		return sb.toString();
	}

	// WAITS
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void waitVisible(AppiumDriver driver, WebElement element, int timeout) {
		FluentWait wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(timeout))
				.pollingEvery(Duration.ofSeconds(200)).ignoring(NoSuchElementException.class);
		wait.until(new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver appiumDriver) {
				return element;
			}
		});
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public boolean waitValueIs(AppiumDriver driver, WebElement element, String value, int time) {
		FluentWait wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(time))
				.pollingEvery(Duration.ofMillis(200)).ignoring(NoSuchElementException.class);
		wait.until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver appiumDriver) {
				return element.getAttribute("value").equals(value);
			}
		});
		return false;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public boolean waitTextContains(AppiumDriver driver, WebElement element, String text, int time) {
		FluentWait wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(time))
				.pollingEvery(Duration.ofMillis(200)).ignoring(NoSuchElementException.class);
		wait.until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver appiumDriver) {
				return element.getAttribute("textContent").contains(text);
			}
		});
		return false;
	}

	public void wait(AppiumDriver appiumDriver, String xpath) {
		try {
			fluentWait(appiumDriver, ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)), 30);
		} catch (Exception e) {
			// Ok
		}
	}

	private boolean isElementDispalyed = false;

	public boolean skipifElementisNotDisplayed() {
		return isElementDispalyed;
	}

	protected WebElement findElement(AppiumDriver appiumDriver, String xpath, boolean isSkip,
			boolean needToScroll) {
		WebElement element = implementationToFindElement(appiumDriver, xpath, isSkip, needToScroll);
		return element;
	}

	/**
	 * used to find element by text Used TouchAction for scrolling to find the
	 * element
	 * 
	 * @param driver
	 * @param xpath
	 * @return WebElement
	 */
	// used to finding the element in screen automatically scrolls full page for
	// finding the element by Text
	protected WebElement implementationToFindElement(AppiumDriver appiumDriver, String xpath,
			boolean isSkipElement, boolean needToScroll) {
		isElementDispalyed = true;
		WebElement element = null;
		int maxSwipes = 4;

		try {
			fluentWait(appiumDriver, ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)), 10);
			element = appiumDriver.findElement(By.xpath(xpath));
		} catch (Exception e) {
			isElementDispalyed = false;

			if (needToScroll) {
				try {
					scrollDown(appiumDriver, xpath);
					for (int i = 0; i < maxSwipes; i++) {
						if (validateElementIsDisplayed(appiumDriver, appiumDriver.findElement(By.xpath(xpath)))) {
							isElementDispalyed = true;
							element = appiumDriver.findElement(By.xpath(xpath));
							break;
						} else {
							scrollDown(appiumDriver, xpath);
						}
					}

				} catch (Exception e1) {
					print("Element not found");
				}

				if (isSkipElement && !isElementDispalyed) {
					// if (extentTest != null)
					// extentTest.log(LogStatus.SKIP, "Skipped Element : " + xpath);
				}
			}
		}

		return element;
	}

	/**
	 * Used to reset app for Android and iOS For iOS need to pass parameter bundleId
	 * For Android need to pass parameter package name
	 */
	public void resetApp() {
		((SupportsContextSwitching) DriverInstance.getDriver()).context("NATIVE_APP");
		if (DriverInstance.getDriver() instanceof AndroidDriver) {
			AndroidDriver driver = ((AndroidDriver) DriverInstance.getDriver());
			String packageName = driver.getCurrentPackage();
			driver.terminateApp(packageName);
			driver.activateApp(packageName);
			driver.rotate(ScreenOrientation.PORTRAIT);
		} else {
			IOSDriver driver = ((IOSDriver) DriverInstance.getDriver());
			if(bundleId == null || bundleId.isEmpty()) {
				bundleId = (String) driver.getCapabilities().getCapability("appium:bundleId");
			}
			driver.terminateApp(bundleId);
			driver.activateApp(bundleId);
		}

	}

	/**
	 * Close current execution driver connection
	 */
	public void closeDriver() {
		try {
			DriverInstance.getDriver().quit();
		} catch (Exception e) {
			// OK
		}
		DriverInstance.setDriver(null);
	}

	/**
	 * used to create xpath by platform
	 * 
	 * @param xpathId
	 * @return final xpath based on platform
	 */
	public String getXpathById(String xpathId) {

		if (DriverInstance.getDriver() instanceof AndroidDriver) {
			return "//*[@resource-id='" + xpathId + "']";
		} else {
			return "//*[@name='" + xpathId + "']";
		}
	}

	/**
	 * used to create xpath by platform and attributes
	 *
	 * @param xpathValue
	 * @return final xpath based on platform
	 */
	public String getXpathByAttribute(String xpathValue, String androidAttribute, String iOSAttribute) {

		if (DriverInstance.getDriver() instanceof AndroidDriver) {
			return "//*[@" + androidAttribute + "='" + xpathValue + "']";
		} else {
			return "//*[@" + iOSAttribute + "='" + xpathValue + "']";
		}
	}

	/**
	 * Returns Webelment of xpath locator.
	 * 
	 * @param xpathLocator - xpath locator of element.
	 * @return - Webelemnt of locator.
	 */
	public WebElement getWebElement(String xpathLocator) {
		return DriverInstance.getDriver().findElement(By.xpath(xpathLocator));
	}

	public void cleanupObject(Object obj) {
		obj = null;
	}

	@AfterSuite
	public void afterSuiteRuns() {
		try {
			MobileDriverInstance.getDriver().close();
			MobileDriverInstance.getDriver().quit();

		} catch (Exception e) {
			// OK
		}

	}

	public String addScreenshot(String base64Data, String title) {
		String html = "<div class=\"row mb-3\"><div class=\"col-md-3\">\r\n" + "        <a href=\"" + base64Data
				+ "\" data-featherlight=\"image\">" + "<span class=\"badge badge-gradient-primary\">" + title
				+ "</span></a>\r\n" + "    </div></div>";
		return html;
	}

	public void scrollDown(AppiumDriver appiumDriver, String xpath, double distance) {
		scroll(ScrollDirection.DOWN, distance);
	}

	public void scrollUp(AppiumDriver appiumDriver, String xpath, double distance) {
		scroll(ScrollDirection.UP, distance);
	}
	
	public boolean skipForMobileExecution(RemoteWebDriver driver, String xpath) {
		if(driver instanceof AppiumDriver) {
			return false;
		}
		return true;
	}

	public boolean skipForWebExecution(RemoteWebDriver driver, String _clearfix) {
		if(driver instanceof AppiumDriver) {
			return true;
		}
		return false;
	}

	public void waitForElement(AppiumDriver driver, String xpath, int timeInSecs) {
		fluentWait(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)), timeInSecs);
	}

}
