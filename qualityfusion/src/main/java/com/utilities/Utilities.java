package com.utilities;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.winium.WiniumDriver;

import com.epam.healenium.SelfHealingDriver;
import com.google.gson.JsonElement;

import io.appium.java_client.AppiumDriver;


public class Utilities extends BaseClass {

	private static boolean isMobile = false;

	//private static final Logger LOGGER = LoggerFactory.getLogger(Utilities.class);
	// Capture Screen Shot and save in the location
	public static String captureScreenshot(WebDriver driver, String screenShotName) {
		try {
			TakesScreenshot ts;
			if (driver instanceof SelfHealingDriver) {
				ts = ((TakesScreenshot) ((SelfHealingDriver) driver).getDelegate());
			} else if (driver instanceof AppiumDriver) {
				System.out.println("=====Print Mobile");
				ts = (TakesScreenshot) ((AppiumDriver) driver);
			} else {
				ts = (TakesScreenshot) driver;
			}

			String base64 = ts.getScreenshotAs(OutputType.BASE64);
			return base64;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}


	public static String takeCanvasScreenshot(WebElement element, String name) {

		String base64 = element.getScreenshotAs(OutputType.BASE64);
		return addScreenshots(base64, name  + "");
	}


	public static String addScreenshots(String base64Data, String title) {
		String html = "<div class=\"row mb-3\"><div class=\"col-md-3\">\r\n"
				+ "        <a href=\"data:image/png;base64,"+base64Data+"\" data-featherlight=\"image\">"
				+ "<span class=\"badge badge-gradient-primary\">" + title + "</span></a>\r\n"
				+ "    </div></div>";
		return html;
	}

	public static String captureScreenshotDesktopApplication(WiniumDriver driver, String screenShotName) {
		String path = "";
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH-mm-ss");
		Date dt = new Date();
		String html = "";
		try {
			/*if(Constants.IS_TESTCASE && Constants.iS_WEB || !Constants.iS_WEB) {
				Constants.TOTAL_TC_FAILED = Constants.TOTAL_TC_FAILED + 1;
				Constants.IS_TESTCASE = false;
			} */

			System.out.println(dateFormat.format(dt));
			TakesScreenshot ts = driver;
			File source = ts.getScreenshotAs(OutputType.FILE);
			html = covertScreenshotToBase64(source , screenShotName);
			path = System.getProperty("user.dir") + File.separator + "screenshots";
			createDirectory(path);
			FileUtils.copyFile(source, new File(path + File.separator  + dateFormat.format(dt) + "_" + screenShotName + ".png"));
			System.out.println("screenshot is taken");

		} catch (Exception e) {
			System.out.println("exception while taking screenshot" + e.getMessage());
		}

		return html;
	}





	@SuppressWarnings("resource")
	public static String covertScreenshotToBase64(File file, String name) {
		try {		
			FileInputStream fis = new FileInputStream(file);
			byte[] byteArray = new byte[(int)file.length()];
			fis.read(byteArray);
			String imageString = Base64.encodeBase64String(byteArray);
			return doImageClickAnimation(imageString, name);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	@SuppressWarnings("unused")
	public static String doImageClickAnimation(String img, String screenName) {

		int width = 500;
		int height = 250;
		if(isMobile) {
			width = 400;
			height = 700;
		} 
		//String image = "<img onclick='clickImage(this)' src=\"data:image/png;base64, " + img + "\" alt=\""+ screenName +"\" width=\"" + width + "\" height=\"" + height + "\"/>";
		return (img);

	}

	// make zip of reports
	/*public static void zip(String filepath) {
		try {
			File inFolder = new File(filepath);
			File outFolder = new File("Reports.zip");
			ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(outFolder)));
			BufferedInputStream in = null;
			byte[] data = new byte[1000];
			String files[] = inFolder.list();
			for (int i = 0; i < files.length; i++) {
				in = new BufferedInputStream(new FileInputStream(inFolder.getPath() + File.separator + files[i]), 1000);
				out.putNextEntry(new ZipEntry(files[i]));
				int count;
				while ((count = in.read(data, 0, 1000)) != -1) {
					out.write(data, 0, count);
				}
				out.closeEntry();
			}
			out.flush();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	public static boolean createDirectory(String directory) {
		File fileDirectory = new File(directory);
		if (!fileDirectory.exists()) {
			fileDirectory.mkdir();
			return true;
		}
		return false;
	}

	@SuppressWarnings("resource")
	public static String getElapsedTime1(String filePath) {
		File file = new File(filePath);
		Scanner in = null;
		try {
			in = new Scanner(file);

			while (in.hasNext()) {
				String line = in.nextLine();
				if (line.contains("suite-total-time-overall-value panel-lead")) {
					return line.split("<span class='suite-total-time-overall-value panel-lead'>")[1].replaceAll("</span>", "");
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return "";
	}



	public static void setMobilePlatform() {
		isMobile  = true;
	}


	public static Object[][] sauceDevicesList() throws Exception {

		List<Object> data = new ArrayList<>(); 
		ConfigFilesUtility  configFileObj = new ConfigFilesUtility();
		configFileObj.loadPropertyFile("mobileconfig.properties");
		String executionDevicePlatform = configFileObj.getProperty("executionPlatform");
		if(executionDevicePlatform.equalsIgnoreCase("local")) {
			configFileObj.loadPropertyFile("DeviceCapabilities.properties");
			Object obj = new Object[]{
					configFileObj.getProperty("deviceName"),
					configFileObj.getProperty("platformName"),
					configFileObj.getProperty("platformVersion"),
					configFileObj.getProperty("appium_version"),
					configFileObj.getProperty("device_orientation"),
					configFileObj.getProperty(configFileObj.getProperty("projectName")),
					configFileObj.getProperty("udid"),
					""
			};
			data.add(obj);
		} else {
			configFileObj = new ConfigFilesUtility();
			configFileObj.loadPropertyFile("ServerDeviceCapabilities.properties");
			String js = configFileObj.getProperty("devices");
			String app = configFileObj.getProperty("app");
			JSONArray devicesArray = new JSONArray(js);

			for(int i=0;i<devicesArray.length();i++) {

				JSONObject devicesObj = devicesArray.getJSONObject(i);
				String deviceName = "";
				if(executionDevicePlatform.equals("saucelabvirtural") || executionDevicePlatform.contains("kobiton")) {
					deviceName = devicesObj.optString("deviceName");		
				} else {
					deviceName = devicesObj.optString("mobileDeviceId");	
				}
				//String appe = "https://github.com/saucelabs-training/demo-java/blob/master/appium-example/resources/android/GuineaPigApp-debug.apk?raw=true";
				Object obj = new Object[]{
						deviceName,
						devicesObj.optString("devicePlatformName"),
						devicesObj.optString("devicePlatformVersion"),
						devicesObj.optString("appium_version","1.9.1"),
						devicesObj.optString("device_orientation","portrait"),
						configFileObj.getProperty("projectName"),
						configFileObj.getProperty("testObjApiKey"),
						app,	 
				};

				data.add(obj);
			}
		}

		Object[][] array = new Object[data.size()][];
		for (int i = 0; i < data.size(); i++) {
			Object row =  data.get(i);
			array[i] = (Object[]) row;
		}
		return  array;
	}


	public static boolean doSaveElementsToServer(String url, String json, boolean isServer) throws Exception {
		try {
			URL obj = new URL(url);
			System.out.println(url);
			TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				@Override
				public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
					// TODO Auto-generated method stub

				}

				@Override
				public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
					// TODO Auto-generated method stub

				}
			} };

			// Install the all-trusting trust manager
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			// Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};
			// Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");
			con.setConnectTimeout(200000);
			con.setConnectTimeout(200000);
			con.setDoOutput(true);
			OutputStream os = con.getOutputStream();
			DataOutputStream wr = new DataOutputStream(os);
			byte[] isoString = json.getBytes(StandardCharsets.UTF_8);

			wr.write(isoString, 0, isoString.length);
			// wr.writeBytes(json);
			wr.flush();
			wr.close();
			os.close();
			con.connect();
			int responseCode = con.getResponseCode();
			System.out.println("Response Code : " + responseCode);
			if(responseCode != 200) {
				System.exit(1);
			}
			
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			System.out.println(response);
			in.close();



			writeFile(json, "Report", System.getProperty("user.dir") + File.separator, ".txt");

			try {
				JSONObject jsonObject = new JSONObject(response.toString());
				if (jsonObject.has("status") && !jsonObject.getString("status").equalsIgnoreCase("SUCCESS")) {
					System.out.println("Report Exception " + jsonObject.optString("message"));
					return false;
				}

				if(jsonObject != null && jsonObject.has("info")) {

					JSONObject object = jsonObject.getJSONObject("info");

					String jsonReport = object.optString("report_json");
					String htmlReport = object.optString("report_html");
					String pdfReport = object.optString("report_pdf");

					String status = object.optString("report_result").contains("0 FAIL") ? "PASS" : "FAIL";

					System.out.println("Test Result : " + object.optString("report_result"));
					System.out.println("JSON Report : " + jsonReport);
					System.out.println("HTML Report : " + htmlReport);
					System.out.println("PDF Report : " + pdfReport);

					System.err.println("STATUS : " + status);


					if(object.has("report_result") && !object.optString("report_result").contains("0 FAIL")) {
					//	Assert.fail("Should not fail Automation Tests. Please check ");
						System.err.println("Build " + status + ": " + object.optString("report_result"));
						if(isServer)
							System.exit(1);
					}

				}
			} catch (Exception e) {
				System.exit(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getLocalizedMessage());
			System.exit(1);
			
		}
		return true;
	}

	private static void writeFile(String testcaseClassStart, String className, String filePath, String fileExtension)
			throws IOException {
		Writer out = new BufferedWriter(
				new OutputStreamWriter(new FileOutputStream(filePath + File.separator + className + fileExtension), "UTF-8"));
		try {
			out.write(testcaseClassStart);
		} finally {
			out.close();
		}
	}

	public static JSONArray getJsonData() {
		return new JSONArray();

	}


	public static String getFinalData(String splitData, ConfigFilesUtility configFileObj) {
		String returnData = splitData;
		try {

			if (splitData.contains("$") && splitData.contains("#")) {

				String[] data = splitData.split("\\$");
				for (int i = 0; i < data.length; i++) {
					if (data[i].startsWith("#") && data[i].endsWith("#")) {
						System.out.println(data[i]);
						String replacement = data[i].replaceAll("#", "");
						if(configFileObj == null) 
							return "";
						String finalConfigData = configFileObj.getProperty(replacement);
						returnData = returnData.replaceAll("\\$", "").replaceAll("#", "").replace(replacement,
								finalConfigData);
					}
				}
			}
		}catch (Exception e) {
			return splitData + "<br><br>Please check ordering the testcases and given parameters is valid or not";
		}
		return returnData;
	}

	static String previousElement = "";
	public static ArrayList<String> getData(String key, JsonElement jsonElement, ArrayList<String> str) {

		if (jsonElement.isJsonArray()) {
			for (JsonElement jsonElement1 : jsonElement.getAsJsonArray()) {
				getData(key, jsonElement1, str);
			}
		} else {
			if (jsonElement.isJsonObject()) {
				Set<Map.Entry<String, JsonElement>> entrySet = jsonElement
						.getAsJsonObject().entrySet();
				for (Map.Entry<String, JsonElement> entry : entrySet) {
					String key1 = entry.getKey();

					if(key.contains(".") && key1.equalsIgnoreCase(key.split("\\.")[1]) && previousElement.equalsIgnoreCase(key.split("\\.")[0])) {
						String value = entry.getValue().toString();

						str.add(value);
					} else {
						if (key1.equalsIgnoreCase(key)) {
							String value = entry.getValue().toString();

							str.add(value);
						}
					}
					previousElement = key1;
					getData(key, entry.getValue(),str);
				}
			} else {
				if (jsonElement.toString().equals(key)) {
					str.add(jsonElement.toString());
				}
			}
		}

		return str;
	}



	public static LinkedHashMap<String, Object> toMap(JSONObject object) throws JSONException {
		LinkedHashMap<String, Object> map = new LinkedHashMap<String, Object>();

		Iterator<String> keysItr = object.keys();
		while(keysItr.hasNext()) {
			String key = keysItr.next();
			Object value = object.get(key);

			if(value instanceof JSONArray) {
				value = toList((JSONArray) value);
			}

			else if(value instanceof JSONObject) {
				value = toMap((JSONObject) value);
			}
			map.put(key, value);
		}
		return map;
	}


	public static List<Object> toList(JSONObject obj) throws JSONException {
		List<Object> list = new ArrayList<Object>();
		list.add(toMap(obj));
		return list;
	}

	public static List<Object> toList(JSONArray array) throws JSONException {
		List<Object> list = new ArrayList<Object>();
		for(int i = 0; i < array.length(); i++) {
			Object value = array.get(i);
			if(value instanceof JSONArray) {
				value = toList((JSONArray) value);
			}

			else if(value instanceof JSONObject) {
				value = toMap((JSONObject) value);
			}
			list.add(value);
		}
		return list;
	}

	public static int randomNumber;
	public static void setRandomNumber(int number) {
		randomNumber = number;
	}

	public static int getRandomNumber() {
		return randomNumber;
	}
	
    public static boolean isFileDownloaded(String expectedFileName, String fileExtension, int timeOut) throws IOException 
    {
       
       // String folderName = System.getProperty("user.dir") + File.separator + "downloads";
        
        String folderName = System.getProperty("user.home") +File.separator +  "Downloads";
          File[] listOfFiles;
          String fileName;   
        boolean fileDownloaded = false;      
        long startTime = Instant.now().toEpochMilli();
        long waitTime = startTime + timeOut;         
        while (Instant.now().toEpochMilli() < waitTime) 
        {             
            listOfFiles = new File(folderName).listFiles();
            for (File file : listOfFiles) 
            {
                fileName = file.getName().toLowerCase();
                System.out.println("The file name is:" + fileName);
            
                if (file.lastModified() > startTime && !fileName.contains("crdownload") &&   fileName.contains(expectedFileName.toLowerCase()) && fileName.contains(fileExtension.toLowerCase())) 
               {
                   fileDownloaded = true;
                   //uncomment if file removal is necessary
                   //file.delete();
                   break;
               }
            }
           if (fileDownloaded) 
                break;
        }
        return fileDownloaded;
    }


}
