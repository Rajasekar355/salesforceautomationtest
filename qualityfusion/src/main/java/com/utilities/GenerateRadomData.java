package com.utilities;

import java.util.Random;

public class GenerateRadomData {
	
	public static String randomNumber = "";
	
	public  static void setRandomNumber(String data) {
		randomNumber = data;
	}
	
	public  static String getRandomNumber() {
		if(randomNumber.isEmpty()) {
			randomNumber =  "" + generateRandomIntRange(100, 9999);
		} 
		return randomNumber;
	}

	
	public static int generateRandomIntRange(int min, int max) {
		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}
}
