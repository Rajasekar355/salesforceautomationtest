package com.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;
public class ConfigFilesUtility {
	/* Set the Configuration setUp path */
	File src;
	FileInputStream fis;
	Properties prop;
	public void loadPropertyFile(String configFileName) {

		try {
			String filesPath = System.getProperty("user.dir") + File.separator + "ConfigFiles" + File.separator;
			src = new File(filesPath + configFileName);
			if(src.exists()) {
				fis = new FileInputStream(src);
				prop = new Properties();
				prop.load(fis);


				if(prop.containsKey("datasets")) {
					String props = prop.getProperty("datasets");

					String[] propsDatasets = props.split(","); 

					for(int i=0; i<propsDatasets.length;i++) {
						Properties properties = new Properties();
						properties.load(new FileInputStream(filesPath + propsDatasets[i]));
						prop.putAll(properties);
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public String getProperty(String propKey) {
		String value =  prop.getProperty(propKey, "");
		if (value.startsWith("$")) {
			value =  prop.getProperty(value.replaceAll("\\$", ""), "");
		} else if(value.equals("randomNumber")) {
			value = GenerateRadomData.getRandomNumber();
		}
		return value;
	}

	public int getIntProperty(String propKey) {
		String value =  prop.getProperty(propKey, "0");
		return Integer.parseInt(value);
	}

	public boolean getBooleanProperty(String propKey) {
		String value =  prop.getProperty(propKey, "true");
		return Boolean.parseBoolean(value);
	}


}
