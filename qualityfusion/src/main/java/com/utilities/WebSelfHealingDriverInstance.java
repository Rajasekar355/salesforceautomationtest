package com.utilities;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;

public class WebSelfHealingDriverInstance {

	private static Map<Integer, WebDriver> remoteDriverMap = new HashMap<Integer, WebDriver>();

	private static boolean isJobStatus;

	@SuppressWarnings("deprecation")
	public static WebDriver getDriver() {
		return remoteDriverMap.get((int)Thread.currentThread().getId());
	}
	@SuppressWarnings("deprecation")
	public static void setDriver(WebDriver driver) {
		remoteDriverMap.put((int)Thread.currentThread().getId(), driver);
	}
	
	public static void setJobStatus(boolean jobStatus) {
		isJobStatus = jobStatus;
	}

	public static boolean getJobStatus() {
		return isJobStatus;
	}
}
