package com.utilities;

import io.appium.java_client.AppiumDriver;

public class MobileDriverInstance {
	
	public static AppiumDriver mobileDriver;
	public static boolean isJobStatus;
	
	public static AppiumDriver getDriver() {
		return mobileDriver;
	}
	
	public static void setDriver(AppiumDriver driver) {
		mobileDriver = driver;
	}
	
	public static void setJobStatus(boolean jobStatus) {
		isJobStatus = jobStatus;
	}

	public static boolean getJobStatus() {
		return isJobStatus;
	}
}
