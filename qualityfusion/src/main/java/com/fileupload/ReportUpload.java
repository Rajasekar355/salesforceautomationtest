package com.fileupload;

import java.io.File;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;

import com.aventstack.extentreports.Status;
import com.configurations.GlobalData;
import com.extentreport.ExtTest;
import com.restassured.services.ReportPaths;
import com.utilities.BaseClass;
import com.utilities.Utilities;

@SuppressWarnings("unused")
public class ReportUpload extends BaseClass {
	String primaryInfo = "";
	private final String projectPath = System.getProperty("user.dir");
	private final String reportsPath = projectPath + File.separator + "APIReports" + File.separator + ReportPaths.reportPathName;
	private String resultCount;
	private String datasetResult;
	private String reportstatus;


	/*
	 * Uploading file to server
	 * @moduleId 
	 * @testCaseId
	 * @userID
	 * @testsetId
	 * @moduleDescription
	 */


	public void call() {
		System.out.println(GlobalData.getReportData());
		System.out.println("PrimaryInfo:" + GlobalData.getPrimaryInfo());
	}

	@AfterSuite
	public void uploadReport() throws Exception {

		try {
			try {
				ExtTest.getTest().getExtent().flush();
				System.out.println("Flushed Extent report");
			} catch (Exception e) {
				// TODO: handle exception
			}
			String path = System.getProperty("user.dir") + File.separator + "extent-reports" + File.separator + "extent-report.html";
			
			//System.out.println(GlobalData.getReportData());
			JSONObject primaryInfoObj = new JSONObject(GlobalData.getPrimaryInfo());
			boolean is_web = primaryInfoObj.optBoolean("is_web");
			String uploadFile = primaryInfoObj.optString("upload_file");

			boolean isDesktopAutomation = primaryInfoObj.optBoolean("is_Desktop_Automation");
			String mobile = primaryInfoObj.optString("mobile_platform");
			String moduleDescription = primaryInfoObj.optString("module_description");
			//System.err.println(Constants.testName);
			String moduleId = primaryInfoObj.optString("module_id");
			String userId = primaryInfoObj.optString("user_id");
			String executedUserId = primaryInfoObj.optString("executed_user_id");
			String executionEnv = primaryInfoObj.optString("execution_environment");
			long startExecutionTime = GlobalData.getStartTime();
			long endExecutionTime = GlobalData.getEndTime();
			String client_timezoneId = primaryInfoObj.optString("client_timezone_id");
			String report_upload_url = primaryInfoObj.optString("report_upload_url");
			String testcaseId = primaryInfoObj.optString("testcase_id");
			//String datasetId = primaryInfoObj.optString("testcase_id");
			String subModuleId = primaryInfoObj.isNull("sub_module_id") ? null : primaryInfoObj.optString("sub_module_id");
			String testsetId = primaryInfoObj.optString("testset_id").equals("0") ? "" : primaryInfoObj.optString("testset_id");
			String testsetName = (primaryInfoObj.optString("testset_name") == null || primaryInfoObj.optString("testset_name").equals("null") || primaryInfoObj.optString("testset_name").isEmpty()) ? "" : primaryInfoObj.optString("testset_name");
			String testcaseName = primaryInfoObj.optString("testcase_name");
			
			JSONArray testcasesArray = GlobalData.getReportData();
			JSONObject primaryInfo = new JSONObject();
			primaryInfo.put("testset_name", testsetName);
			//String elapsedTime = Utilities.getElapsedTime(reportPath);
			//MultipartUtility multipart = new MultipartUtility(reportUploadURL, charset);
			primaryInfo.put("user_id", userId);
			String reportName = "";
			try {
				String testcasName = "";
				if (!testsetId.isEmpty()) {
					testcasName = testsetName;
				} else {
					testcasName = testcaseName;
				}
				//reportName = MultipartFileUploader.uploadFile(path, uploadFile, testcaseName);
			} catch (Exception e) {
				System.err.println("File not uploaded" + e.getLocalizedMessage());
			}
			
			primaryInfo.put("report_path", reportName);
			primaryInfo.put("executed_user_id", executedUserId);
			if (!testsetId.isEmpty()) {
				primaryInfo.put("testset_id", testsetId);
			} else {
				primaryInfo.put("testcase_id", testcaseId);
			}

			if(mobile != null && (mobile.equalsIgnoreCase("Android")||mobile.equalsIgnoreCase("iOS"))) {
				primaryInfo.put("report_type", mobile);
			} else if(isDesktopAutomation) {
				primaryInfo.put("report_type", "winium");
			} else {
				primaryInfo.put("report_type", is_web ? "web" : "api");
			} 

			primaryInfo.put("module_id", (subModuleId == null || subModuleId.isEmpty() || subModuleId.equals("0")) ? moduleId : subModuleId);
			//primaryInfo.put("report_result", result);
			//primaryInfo.put("dataset_result", datasetResult);
			primaryInfo.put("start_time", String.valueOf(startExecutionTime));
			primaryInfo.put("end_time", String.valueOf(endExecutionTime));

			long elapsedtime = startExecutionTime- endExecutionTime;
			primaryInfo.put("execution_time", String.valueOf(elapsedtime));
			//primaryInfo.put("report_status", reportstatus);
			primaryInfo.put("client_time_zone_id", client_timezoneId);
			//System.out.println("client_time_zone_id"+  client_timezoneId);
			//primaryInfo.put("report_data", GlobalData.getReportData().toString());
			primaryInfo.put("testcases_result", testcasesArray);
			//primaryInfo.put("testcases_result", testcasesArray);



			if(!executionEnv.equalsIgnoreCase("local")) {
				System.out.println("REPORT_START\n" + primaryInfo.toString() + "\nREPORT_END");
				Utilities.doSaveElementsToServer(report_upload_url, primaryInfo.toString(), true);
			} else {
				Utilities.doSaveElementsToServer(report_upload_url, primaryInfo.toString(), false);
			}
			
			

			

			
		//	Assert.assertEquals(is_web, 0);

			//new FileUploaderClient().uploadFile(report_upload_url, reportsPath, userId,executedUserId, testcaseId, testsetId, moduleId, subModuleId, is_web, resultCount, GlobalData.getReportData().toString(), mobile, client_timezoneId,datasetResult,false, startExecutionTime,endExecutionTime);


		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			//e.printStackTrace();
		}
	}




}
