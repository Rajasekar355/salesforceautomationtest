package com.fileupload;

import java.io.File;

import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class MultipartFileUploader {

	public static String uploadFile(String filePath, String requestURL, String name) {
		
		File file = new File(filePath);
		
		OkHttpClient client = new OkHttpClient().newBuilder().build();

		RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
				.addFormDataPart("file", file.getAbsolutePath(),RequestBody.create(MediaType.parse("application/octet-stream"), new File(file.getAbsolutePath())))
				.addFormDataPart("name", name)
				.build();
		Request request = new Request.Builder()
				.url(requestURL)
				.method("POST", body)
				.build();
		try {
			Response response = client.newCall(request).execute();
			JSONObject jsonObj = new JSONObject(response.body().string());
			String reportName = jsonObj.getString("info");
			System.err.println("Report uploaded: " + reportName);
			System.err.println("Name: " + name);		
			return reportName;
		} catch (Exception e) {
			System.err.println("File not uploaded: " + e.getLocalizedMessage());
		}
	
		return "";
	}
}