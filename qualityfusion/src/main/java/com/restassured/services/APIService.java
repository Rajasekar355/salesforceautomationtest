package com.restassured.services;

import static io.restassured.RestAssured.given;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.text.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.testng.annotations.Listeners;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.CodeLanguage;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.configurations.GlobalData;
import com.customcode.QFCustomizedCode;
import com.epam.reportportal.testng.ReportPortalTestNGListener;
import com.extentreport.ExtTest;
import com.utilities.ConfigFilesUtility;
import com.utilities.LoggingUtils;
import com.utilities.MyApp;
import com.utilities.OAuthUtilities;
import com.utilities.QFUtilities;
import com.utilities.ReportPortalBaseClass;
import com.utilities.Utilities;

import io.restassured.config.EncoderConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;
import okhttp3.Credentials;
@Listeners({ReportPortalTestNGListener.class})
public class APIService extends ReportPortalBaseClass {

	private static JSONArray jsonArray;
	private static ConfigFilesUtility configFileObj;
	private static boolean isXMLData = false;
	
	public static String BASE_URL= "";

	
	@SuppressWarnings({ "unused", "static-access" })
	public static String callRequest(ConfigFilesUtility con,
			String apiName,
			String urlParams,
			String headers, int requestType, int bodyType, String inputBody,
			String datatsetHeader, String dataResources, String authenticationData,String formurlEncodedData,
			String formData,String savedParameters, String statusParameters) {
		

		configFileObj= new ConfigFilesUtility();
		isXMLData = false; 

		//String[] bodyTpes = new String[] { "", "form-data", "x-www-form-urlencoded", "raw" };
		String[] types = new String[] { "", "GET", "POST", "PUT", "DELETE", "PATCH" };
		String contentType = "";
		JSONObject jsonoBj = new JSONObject();
		jsonArray = new JSONArray();
		String responseString = "";
		try {
			String data  = con.getProperty("PrimaryInfo");
			JSONObject jsonObject = new JSONObject(data);
			String testCaseName = jsonObject.getString("testcase_name");
			String projectName = QFUtilities.removeUnWantedChars(jsonObject.optString("project_name")).replaceAll(" ", "").toLowerCase();
			String projectId = jsonObject.optString("project_id");
			String reportsPath = "reportsPath";
			try {
				configFileObj.loadPropertyFile(  projectName +".properties");	
			} catch (Exception e) {
				//e.printStackTrace();
			}

			String returnString = jsonObject.optString("returnString");
			String packageFolder = jsonObject.optString("moduleName");
			String type = types[requestType];
			jsonoBj.put("testcase_name",testCaseName + "-" + datatsetHeader + "-" + apiName);
			
			jsonoBj.put("datasets", jsonArray);
			new GlobalData().reportData(testCaseName, jsonoBj);
			new GlobalData().primaryInfoData(con);
			String Url = getFinalData(jsonObject.optString("project_url") + dataResources);
			printApiHeader("API Name : " + apiName + "-" + datatsetHeader);
			reportCreation("info","API Name : " + testCaseName + "-" + datatsetHeader + "-" + apiName);
			reportCreation("info",Url);
			BASE_URL = Url;
			

			RequestSpecification requestSpec = given();
			requestSpec.config(RestAssuredConfig.config().encoderConfig(EncoderConfig.encoderConfig().defaultContentCharset("UTF-8")));
			if(authenticationData != null && !authenticationData.isEmpty()) {
				JSONObject authData = new JSONObject(authenticationData);
				String authType = authData.optString("authtype");

				if(authType.equalsIgnoreCase("oauth2")) {
					String response = "";
					try {
						JSONObject obj = authData.optJSONObject("oauth2"); 
						String tokenURL = obj.optString("tokenurl");
						String clientId = obj.optString("clientid");
						String clientSecret = obj.optString("clientsecret");
						reportCreation("info", "Token URL: " + tokenURL);
						reportCreation("info", "Client Id: " + clientId);
						reportCreation("info", "Client Secret: " + clientSecret);
						response=  OAuthUtilities.getAccessTokenFromOAuthData(tokenURL, clientId, clientSecret);
						MyApp.data(projectName, response, savedParameters, "[]");
						reportCreation("info", "Response: " + response);
					} catch (Exception e) {
						reportCreation("fail", "Invalid response : " + e.getLocalizedMessage());
					}

					return response;
				}

				if (authType.equalsIgnoreCase("bearertoken")) {
					JSONObject bearerTokenObj = authData.optJSONObject("bearertoken");
					String token = getFinalData(bearerTokenObj.optString("token"));

					requestSpec.header("Authorization", "Bearer " + token);
					reportCreation("info", "Authorization : Bearer " + token);
				} else if (authType.equalsIgnoreCase("basicauth")) {
					JSONObject basicauthObj = authData.optJSONObject("basicauth");
					String username = getFinalData(basicauthObj.optString("username"));
					String password = getFinalData(basicauthObj.optString("password"));
					String credentials = Credentials.basic(username, password);
					requestSpec.relaxedHTTPSValidation().header("Authorization", credentials);
					reportCreation("info", "Username : " + username);
					reportCreation("info", "Password : "  + password);
				
				} else if (authType.equalsIgnoreCase("apikey")) {

					JSONObject apiKeyObj = authData.optJSONObject("apikey");
					String apikey = getFinalData(apiKeyObj.optString("key"));
					String value = getFinalData(apiKeyObj.optString("value"));

					reportCreation("info", "apikey : " + apikey + "\n value : "  + value);
					String headerOrQuery = apiKeyObj.optString("value");
					if (headerOrQuery.equalsIgnoreCase("header")) {
						requestSpec.header(apikey, value);
					} else {
						requestSpec.queryParam(apikey, value);
					}
				}
			}


			JSONArray headersJsonArray = new JSONArray(headers);
			JSONArray parameters = new JSONArray(urlParams);
			JSONObject body = new JSONObject(inputBody);
			if(body.toString().contains("#Random")) {
				int randomNumber = generateRandomIntRange(1000, 9999);
				Utilities.setRandomNumber(randomNumber);
				body = new JSONObject(body.toString().replaceAll("#Random", "" + Utilities.getRandomNumber()));
			}
			
			
			int raw_id = body.optInt("raw_id"); // content type
			reportCreation("info", "Request Type :  " + type);
			contentType = (raw_id == 5 ? "" : "application/json; charset=UTF-8");
			if(!headersJsonArray.toString().contains("content-type")) {
			if (!contentType.isEmpty()){
				reportCreation("info", "Content Type :  " + contentType);
			}
			if (!Url.contains("@"))
				requestSpec.header("Content-Type", contentType);
			}
			System.out.println("Request Type :  " + type);
			 QFCustomizedCode.bodyParams(apiName, body, contentType, projectName);

			if (headersJsonArray.length() > 0) {
				extentHeaderLog( "Headers");
				System.out.println("Headers :  " + StringEscapeUtils.unescapeJava(headersJsonArray.toString()));
			} 

			for (int i = 0; i < headersJsonArray.length(); i++) {
				JSONObject headerObj = headersJsonArray.getJSONObject(i);
				String headerkey = getFinalData(headerObj.getString("header_key"));
				String headerValue = getFinalData(headerObj.getString("header_value"));
				reportCreation("info", headerkey + " : "+ headerValue);
				requestSpec.header(headerkey, headerValue);
			}

			if (parameters.length() > 0) {
				extentHeaderLog( "Input Parameters");
				System.out.println("Parameters :  " + StringEscapeUtils.unescapeJava(parameters.toString()));
			} 

			for (int i = 0; i < parameters.length(); i++) {
				JSONObject parametersObj = parameters.getJSONObject(i);
				String key = getFinalData(parametersObj.getString("param_key")).replaceAll("\n", "");
				String value = QFCustomizedCode.updateApiInputParams(apiName,key);
				if(value == null || value.isEmpty())
					value = getFinalData(parametersObj.getString("param_value")).replaceAll("\n", "");
				if (requestType > 1) {
					reportCreation("info", key + " : "+ value);
					requestSpec.queryParam(key, value);
				} else {
					reportCreation("info", key + " : "+ value);
					requestSpec.param(key, value);
				}
			}

			

			Response response = null;
			if (requestType == 1) { // GET
				response = requestSpec.relaxedHTTPSValidation().when().get(Url).then().extract().response();
			} else if (requestType > 1) { // POST,PUT,DELETE
				String rawBody = "";
				JSONArray bodyArray = null;
				if (bodyType == 1 || bodyType == 2) { // form-data or x-www-form-urlencoded
					if(bodyType == 2) {
						contentType =  "application/x-www-form-urlencoded; charset=UTF-8";
						bodyArray = new JSONArray(formurlEncodedData);
					}
					if(bodyType == 1) {
						bodyArray = body.optJSONArray(formData);
					}
					for (int i = 0; i < bodyArray.length(); i++) {
						JSONObject bodyObj = bodyArray.getJSONObject(i);

						String key = getFinalData(bodyObj.optString("key")).replaceAll("\n", "");
						String value =getFinalData(bodyObj.optString("value")).replaceAll("\n", "");
						reportCreation("info", key + " : " + value);
						requestSpec.formParam(key, value);
					}
				} else if (bodyType == 3) { // raw data
					rawBody = getFinalData(body.optString("raw_text"));
					System.out.println("raw body :  " + StringEscapeUtils.unescapeJava(rawBody));
					if (body.length() > 0) {
						extentHeaderLog( "Input Body");
						reportCreation("info", "body  :  " + rawBody.toString());
						System.out.println("body :  " + StringEscapeUtils.escapeHtml3(rawBody.toString()));
					}
				}
				
				


				//requestSpec;
				if(bodyType > 2) {
					requestSpec.relaxedHTTPSValidation().body(rawBody);
				}
				if (requestType == 2) {
					response = requestSpec.relaxedHTTPSValidation().when().post(Url);
				} else if (requestType == 3) {
					response =  requestSpec.relaxedHTTPSValidation().when().put(Url);
				} else if (requestType == 4) {
					response =  requestSpec.relaxedHTTPSValidation().when().delete(Url);
				} else if (requestType == 5) {
					response =  requestSpec.relaxedHTTPSValidation().when().patch(Url);
				} 
			}

			extentHeaderLog( "Output");
			if (response != null) {	

				ResponseBody<?> responseBody = response.getBody();
				int statusCode = response.getStatusCode();
				responseString = response.asString();
				System.out.println(responseString);
				String finalResponse = responseString;
				try {
					String cType = response.getContentType().toLowerCase();
					if(cType.contains("xml")) {
						isXMLData = true;
						reportCreation("info", "Response Content-Type: " + cType);
						JSONObject js = XML.toJSONObject(response.asString());
						finalResponse = js.toString();
					}
				} catch (Exception e) {
					finalResponse =responseString;
				}

				boolean status = MyApp.checkStatusCode("" + statusCode, statusParameters);
				if(!status) {
					if (statusCode == 200 || statusCode == 201) {
						
						reportCreation("info",  "Response: " + responseString);
						//reportResponseCreation("info",  "Response: " + responseString);
						MyApp.data(projectName, finalResponse, savedParameters, "[]");

						String validatedResponse = MyApp.data(projectName, finalResponse, "[]", statusParameters );
						if(validatedResponse.isEmpty()) {
							reportResponseCreation("pass", responseString);
							//reportCreation("pass", responseString);
						} else {
							reportResponseCreation("fail",  responseString);
							reportCreation("fail",  validatedResponse);
						}

						System.out.println(testCaseName + " API status code is :" + statusCode);
						return responseString;
					} else if(statusCode == 400) {	
						reportCreation("fail", responseString);
						reportResponseCreation("fail", responseString);
						System.out.println("response :  " + responseString +"status :" + statusCode);
						return responseString;				
					} else if(statusCode == 404) {
						reportCreation("fail", "Invalid response : HTTP Status 404  Not Found ");
						System.out.println("Invalid response body returned as :  " + responseString);
					} else {
						reportCreation("fail", responseString);
						reportResponseCreation("fail", responseString);
					}
				} else {
					reportCreation("info", responseString);
					reportResponseCreation("info", responseString);
				}

				System.out.println(responseString);
			}

		} catch (Exception e) {
			String exception = e.getClass().getSimpleName() + "-" + e.getLocalizedMessage();
			reportCreation("fail", "Invalid response : " + exception);

		}
		return responseString;

	}

	private static void reportResponseCreation(String result, String response) {
		if(response.isEmpty()) return;
		Markup res = MarkupHelper.createCodeBlock(response, CodeLanguage.JSON);
		ExtTest.getTest().info("<b>Pretty Format</b>");
		if(result.equalsIgnoreCase("fail")) {
			ExtTest.getTest().fail(res);
		} else if(result.equalsIgnoreCase("info")) {
			ExtTest.getTest().info(res);
		} else {
			ExtTest.getTest().pass(res);
		}
		
	}

	public static void extentReportLog(String data) {
		reportCreation("info", StringEscapeUtils.unescapeJava(data));
	}

	public static void extentHeaderLog(String data) {
		reportCreation("info", "<b>" + StringEscapeUtils.unescapeJava(data) + "</b>");
	}


	public static boolean isJSONValid(String json) {
		try {
			new JSONObject(json);
		} catch (JSONException ex) {
			try {
				new JSONArray(json);
			} catch (JSONException exception) {
				return false;
			}
		}
		return true;
	}


	public static boolean isXMLLike(String inXMLStr) {

		boolean retBool = false;
		Pattern pattern;
		Matcher matcher;
		// REGULAR EXPRESSION TO SEE IF IT AT LEAST STARTS AND ENDS
		// WITH THE SAME ELEMENT
		final String XML_PATTERN_STR = "<(\\S+?)(.*?)>(.*?)</\\1>";
		// IF WE HAVE A STRING
		if (inXMLStr != null && inXMLStr.trim().length() > 0) {
			// IF WE EVEN RESEMBLE XML
			if (inXMLStr.trim().startsWith("<")) {

				pattern = Pattern.compile(XML_PATTERN_STR,
						Pattern.CASE_INSENSITIVE | Pattern.DOTALL | Pattern.MULTILINE);

				// RETURN TRUE IF IT HAS PASSED BOTH TESTS
				matcher = pattern.matcher(inXMLStr);
				retBool = matcher.matches();
			}
			// ELSE WE ARE FALSE
		}
		return retBool;
	}

	public static void reportCreation(String result, String data) {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("result_type", result);
		jsonObject.put("text", isXMLData ? StringEscapeUtils.unescapeXml(data) :  StringEscapeUtils.unescapeJava(data));

		if(result.equalsIgnoreCase("fail")) {
			System.err.println(StringEscapeUtils.unescapeJava(data));
			LoggingUtils.logFail(result + ": " + StringEscapeUtils.unescapeJava(data));
			ExtTest.getTest().log(Status.FAIL, "<p style=\"color:red\"><b>"+data+ "</b></p>");
		} else if(result.equalsIgnoreCase("info")) {
			LoggingUtils.logInfo(result + ": " + StringEscapeUtils.unescapeJava(data));
			ExtTest.getTest().log(Status.INFO, "<p style=\"color:purple\"><b>"+data+ "</b></p>");
		} else {
			System.out.println((StringEscapeUtils.unescapeJava(data)));
			LoggingUtils.logInfo(result + ": " + StringEscapeUtils.unescapeJava(data));
			ExtTest.getTest().log(Status.PASS, "<p style=\"color:purple\"><b>"+data+ "</b></p>");
		}
		jsonArray.put(jsonObject);
	}




	private static String getFinalData(String splitData) {
		String returnData = splitData;
		try {

			if (splitData.contains("$") && splitData.contains("#")) {

				String[] data = splitData.split("\\$");
				for (int i = 0; i < data.length; i++) {
					if (data[i].startsWith("#") && data[i].endsWith("#")) {
						System.out.println(data[i]);
						String replacement = data[i].replaceAll("#", "");
						if(configFileObj == null) 
							return "";
						String finalConfigData = configFileObj.getProperty(replacement);
						returnData = returnData.replaceAll("\\$", "").replaceAll("#", "").replace(replacement,
								finalConfigData);
					}
				}
			}
		}catch (Exception e) {
			return splitData + "<br><br>Please check ordering the testcases and given parameters is valid or not";
		}
		return returnData;
	}

	public static int generateRandomIntRange(int min, int max) {
		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}

	
	public static void printApiHeader(String data) {
		ExtTest.createNode(data);
	}

	public static void startTestcaseReport(String testcaseName) {
		
		ExtTest.startTest(testcaseName,"");
	}
}
