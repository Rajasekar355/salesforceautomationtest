package com.restassured.services;

import org.json.JSONArray;
import org.json.JSONObject;

import com.utilities.ConfigFilesUtility;

import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Request.Builder;
import okhttp3.RequestBody;
import okhttp3.Response;

public class APIService2 {

	public ConfigFilesUtility configFileObj = null;

	public APIService2() {
		configFileObj = new ConfigFilesUtility();
	}

	public Response getRequest(String url, String contentType, String bodyType, JSONArray inputParams, String inputBody,
			JSONArray headers) {
		try {
			RequestBody body = null;
			OkHttpClient client = new OkHttpClient().newBuilder().build();
			MediaType mediaType = MediaType.parse("application/json");
			if (bodyType == null || bodyType.isEmpty()) {
				inputBody = "";
			} else if (bodyType.equalsIgnoreCase("form-data")) {
				okhttp3.MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);

				for (int i = 0; i < inputParams.length(); i++) {
					JSONObject bodyObj = inputParams.getJSONObject(i);

					String key = getFinalData(bodyObj.optString("key")).replaceAll("\n", "");
					String value = getFinalData(bodyObj.optString("value")).replaceAll("\n", "");
					builder.addFormDataPart(key, value);
				}

				body = builder.build();
			} else if (bodyType.equalsIgnoreCase("QueryParams")) {

				HttpUrl.Builder urlBuilder = HttpUrl.parse(url).newBuilder();

				for (int i = 0; i < inputParams.length(); i++) {
					JSONObject bodyObj = inputParams.getJSONObject(i);

					String key = getFinalData(bodyObj.optString("key")).replaceAll("\n", "");
					String value = getFinalData(bodyObj.optString("value")).replaceAll("\n", "");
					urlBuilder.addQueryParameter(key, value);
				}

				url = urlBuilder.build().toString();
			} else if (bodyType.equalsIgnoreCase("application/x-www-form-urlencoded")) {

				HttpUrl.Builder builder = HttpUrl.parse("").newBuilder();

				for (int i = 0; i < inputParams.length(); i++) {
					JSONObject bodyObj = inputParams.getJSONObject(i);

					String key = getFinalData(bodyObj.optString("key")).replaceAll("\n", "");
					String value = getFinalData(bodyObj.optString("value")).replaceAll("\n", "");
					builder.addQueryParameter(key, value);
				}

				inputBody = builder.build().toString();
				body = RequestBody.create(mediaType, inputBody);
			} else {
				body = RequestBody.create(mediaType, inputBody);
			}

			Builder requestBuilder = new Request.Builder().url(url).method("GET", body);

			for (int i = 0; i < headers.length(); i++) {
				JSONObject headerObj = headers.getJSONObject(i);
				String headerkey = getFinalData(headerObj.getString("header_key"));
				String headerValue = getFinalData(headerObj.getString("header_value"));
				requestBuilder.addHeader(headerkey, headerValue);
			}

			Response response = client.newCall(requestBuilder.build()).execute();

			return response;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private String getFinalData(String splitData) {
		String returnData = splitData;
		try {

			if (splitData.contains("$") && splitData.contains("#")) {

				String[] data = splitData.split("\\$");
				for (int i = 0; i < data.length; i++) {
					if (data[i].startsWith("#") && data[i].endsWith("#")) {
						System.out.println(data[i]);
						String replacement = data[i].replaceAll("#", "");
						if (configFileObj == null)
							return "";
						String finalConfigData = configFileObj.getProperty(replacement);
						returnData = returnData.replaceAll("\\$", "").replaceAll("#", "").replace(replacement,
								finalConfigData);
					}
				}
			}
		} catch (Exception e) {
			return splitData + "<br><br>Please check ordering the testcases and given parameters is valid or not";
		}
		return returnData;
	}

}