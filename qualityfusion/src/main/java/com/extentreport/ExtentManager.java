package com.extentreport;

import java.util.Calendar;
import java.util.TimeZone;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;

public class ExtentManager {
	public static Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
	public static long time = cal.getTimeInMillis();
	public static ExtentReports extentReports;

	public synchronized static ExtentReports createExtentReports() {

		if (extentReports == null) {
			try {
				extentReports = new ExtentReports();
				ExtentSparkReporter reporter = new ExtentSparkReporter("./extent-reports/extent-report.html");
				reporter.config().setReportName("Quality Fusion Report");
				reporter.config().setDocumentTitle("Quality Fusion Report");
				extentReports.attachReporter(reporter);
			} catch (Exception e) {
				// OK
			}

		}
		return extentReports;
	}

}
