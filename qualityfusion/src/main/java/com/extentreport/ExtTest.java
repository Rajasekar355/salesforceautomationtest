package com.extentreport;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

@SuppressWarnings("deprecation")
public class ExtTest {
	public static Map<Integer, ExtentTest> extentTestMap = new HashMap<>();
	public static Map<Integer, ExtentTest> extentTestBaseNodeMap = new HashMap<>();
	public static Map<Integer, JSONArray> mainTestMap = new HashMap<>();
	public static ExtentReports extent = ExtentManager.createExtentReports();
	
	public static synchronized ExtentTest getTest() {
		return extentTestMap.get((int)Thread.currentThread().getId());
	}
	
	public static synchronized JSONArray getJsonArray() {
		return mainTestMap.get((int)Thread.currentThread().getId());
	}


	public static synchronized void endTest() {
		extent.flush();
	}

	public static synchronized ExtentTest startTest(String testName, String browserName) {
		ExtentTest test = extent.createTest(testName, "");
		extentTestMap.put((int) Thread.currentThread().getId(), test);
		extentTestBaseNodeMap.put((int) Thread.currentThread().getId(), test);
		mainTestMap.put((int) Thread.currentThread().getId(), new JSONArray());
		return test;
	}
	
	

	public static void passReport(String message) {
		ExtTest.getTest().log(Status.PASS, message);
	}

	public static void failReport(String message) {
		ExtTest.getTest().log(Status.FAIL, message);
	}

	public static void infoReport(String message) {
		ExtTest.getTest().log(Status.INFO, message);
	}

	public static void createNode(String nodeName) {
		extentTestMap.replace((int) Thread.currentThread().getId(), extentTestBaseNodeMap.get((int) Thread.currentThread().getId()).createNode(nodeName));
	}

}
